﻿using System;
using System.Windows.Forms;

namespace UserControlTest
{
    public partial class FormUsercControl : Form
    {
        public FormUsercControl()
        {
            InitializeComponent();

            UsrCtrAddresses uscAddresses = new UsrCtrAddresses();
            uscAddresses.Dock = DockStyle.Fill;
            // this.Controls.Add(uscAddresses);
            tbpAddresses.Controls.Add(uscAddresses);
        }

        private void btnAddressDialog_Click(object sender, EventArgs e)
        {
            AddressDialog dlgAddress = new AddressDialog();
            dlgAddress.ShowDialog();
        }
    }
}
