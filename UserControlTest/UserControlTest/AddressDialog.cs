﻿using System.Windows.Forms;

namespace UserControlTest
{
    public partial class AddressDialog : Form
    {
        public AddressDialog()
        {
            InitializeComponent();
            UsrCtrAddresses uscAddresses = new UsrCtrAddresses();
            uscAddresses.Dock = DockStyle.Fill;
            this.Controls.Add(uscAddresses);
        }
    }
}
