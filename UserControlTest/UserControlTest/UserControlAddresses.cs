﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace UserControlTest
{
    public partial class UsrCtrAddresses : UserControl
    {
        DataClassesDataContext dataContext = new DataClassesDataContext();

        public UsrCtrAddresses()
        {
            InitializeComponent();
            dgvAddresses.DataSource = dataContext.GetTable<Address>();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var query = from address in dataContext.GetTable<Address>() select address;

            if (!string.IsNullOrWhiteSpace(txbStreet.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txtBoxTitle.Text) );
                query = query.Where(address => address.street.Contains(txbStreet.Text));
            }

            if (!string.IsNullOrWhiteSpace(txbNumber.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txtBoxTitle.Text) );
                query = query.Where(address => address.number.Contains(txbNumber.Text));
            }

            if (!string.IsNullOrWhiteSpace(txbZip.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txbZip.Text) );
                query = query.Where(address => address.zip.Contains(txbZip.Text));
            }

            if (!string.IsNullOrWhiteSpace(txbCountry.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txbCountry.Text) );
                query = query.Where(address => address.country.Contains(txbCountry.Text));
            }

            if (!string.IsNullOrWhiteSpace(txbStreet.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txtBoxTitle.Text) );
                query = query.Where(address => address.street.Contains(txbStreet.Text));
            }

            dgvAddresses.DataSource = query;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dataContext.SubmitChanges();
                MessageBox.Show("Data was saved successfully!", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }
    }
}
