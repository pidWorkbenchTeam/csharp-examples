﻿using System.Windows.Forms;

namespace UserControlTest
{
    partial class FormUsercControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcAddresses = new System.Windows.Forms.TabControl();
            this.tbpAddresses = new System.Windows.Forms.TabPage();
            this.tbpDialog = new System.Windows.Forms.TabPage();
            this.btnAddressDialog = new System.Windows.Forms.Button();
            this.tbcAddresses.SuspendLayout();
            this.tbpDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbcAddresses
            // 
            this.tbcAddresses.Controls.Add(this.tbpAddresses);
            this.tbcAddresses.Controls.Add(this.tbpDialog);
            this.tbcAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcAddresses.Location = new System.Drawing.Point(0, 0);
            this.tbcAddresses.Name = "tbcAddresses";
            this.tbcAddresses.SelectedIndex = 0;
            this.tbcAddresses.Size = new System.Drawing.Size(863, 483);
            this.tbcAddresses.TabIndex = 0;
            // 
            // tbpAddresses
            // 
            this.tbpAddresses.Location = new System.Drawing.Point(4, 25);
            this.tbpAddresses.Name = "tbpAddresses";
            this.tbpAddresses.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAddresses.Size = new System.Drawing.Size(855, 454);
            this.tbpAddresses.TabIndex = 0;
            this.tbpAddresses.Text = "Addresses";
            this.tbpAddresses.UseVisualStyleBackColor = true;
            // 
            // tbpDialog
            // 
            this.tbpDialog.Controls.Add(this.btnAddressDialog);
            this.tbpDialog.Location = new System.Drawing.Point(4, 25);
            this.tbpDialog.Name = "tbpDialog";
            this.tbpDialog.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDialog.Size = new System.Drawing.Size(855, 454);
            this.tbpDialog.TabIndex = 1;
            this.tbpDialog.Text = "Dialog";
            this.tbpDialog.UseVisualStyleBackColor = true;
            // 
            // btnAddressDialog
            // 
            this.btnAddressDialog.Location = new System.Drawing.Point(347, 143);
            this.btnAddressDialog.Name = "btnAddressDialog";
            this.btnAddressDialog.Size = new System.Drawing.Size(217, 99);
            this.btnAddressDialog.TabIndex = 0;
            this.btnAddressDialog.Text = "Address Dialog";
            this.btnAddressDialog.UseVisualStyleBackColor = true;
            this.btnAddressDialog.Click += new System.EventHandler(this.btnAddressDialog_Click);
            // 
            // FormUsercControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 483);
            this.Controls.Add(this.tbcAddresses);
            this.Name = "FormUsercControl";
            this.Text = "UserControlTest";
            this.tbcAddresses.ResumeLayout(false);
            this.tbpDialog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tbcAddresses;
        private TabPage tbpAddresses;
        private TabPage tbpDialog;
        private Button btnAddressDialog;
    }
}

