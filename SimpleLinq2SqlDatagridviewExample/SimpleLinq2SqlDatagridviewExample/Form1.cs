﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimpleLinq2SqlDatagridviewExample
{
    public partial class BookForm : Form
    {
        DatabaseClassesDataContext dataContext = new DatabaseClassesDataContext();
        public BookForm()
        {
            InitializeComponent();
            this.dataGridViewBooks.DataSource = dataContext.GetTable<Book>(); //.GetNewBindingList();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dataContext.SubmitChanges();
                MessageBox.Show("Data was saved successfully!", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }
    }
}
