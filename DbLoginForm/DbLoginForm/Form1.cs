﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;

namespace DbLoginForm
{
    public partial class DbLoginForm : Form
    {
        public DbLoginForm()
        {
            InitializeComponent();
            this.pbLoginStatus.Image = new Bitmap(@"..\..\key.png");
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            computerName_ = System.Windows.Forms.SystemInformation.ComputerName;
            //MessageBox.Show(computerName_);
            SqlConnection scn = new SqlConnection();
            scn.ConnectionString = "Data Source=" + computerName_ + "\\SQLEXPRESS;Initial Catalog=binary_credentials;database=login;integrated security=SSPI";
            //string sqlStr = "INSERT INTO dbo.binary_credentials (username, password) VALUES (\'" + tbUsername.Text + "\', HASHBYTES(\'SHA2_512\', \'" + tbPassword.Text + "\'))";

            string sqlStr = "SELECT COUNT (*) AS cnt FROM dbo.binary_credentials WHERE username='" + tbUsername.Text + "\' AND password=HASHBYTES('SHA2_512', \'" + tbPassword.Text + "\')";

            MessageBox.Show(sqlStr);
            SqlCommand scmd = new SqlCommand(sqlStr, scn);

            //System.IO.File.WriteAllText(@".\WriteText.txt", tbPassword.Text);

            scn.Open();
            
            if (scmd.ExecuteScalar().ToString() == "1")
            {
                this.pbLoginStatus.Image = new Bitmap(@"..\..\open.jpg");
                tbUsername.Clear();
                tbPassword.Clear();
            }
            else
            {
                this.pbLoginStatus.Image = new Bitmap(@"..\..\closed.jpg");
                tbUsername.Clear();
                tbPassword.Clear();
            }
            
            scn.Close();
        }
    }
}
