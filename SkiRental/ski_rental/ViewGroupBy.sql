SELECT ArticleNr, COUNT(ArticleNr) as RentalCount, YEAR(RentalDate) as year, MONTH(RentalDate) as month 
FROM ArticlesToCustomers
GROUP BY MONTH(RentalDate), YEAR(RentalDate), ArticleNr