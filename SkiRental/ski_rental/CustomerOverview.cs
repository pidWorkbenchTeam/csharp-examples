﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ski_rental
{
    public partial class CustomerOverview : Form
    {
        DataClassesDataContext dataContext;

        public CustomerOverview()
        {
            InitializeComponent();
        }

        private void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                try
                {
                    var queryCustomer = (from customer in dataContext.Customers
                                         where customer.CustomerID == Int32.Parse(txtCustomerID.Text)
                                         select customer);

                    dgvCustomers.DataSource = queryCustomer;
                }
                catch
                {
                    MessageBox.Show("Bitte geben Sie eine Nummer ein");
                }
            }
        }

        private void btnShowAllCustomers_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {               
                dgvCustomers.DataSource = dataContext.Customers;
            }
        }

        private void btnCreateCustomer_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                Customers customer = new Customers();

                customer.FirstName = txtFName.Text;
                customer.LastName = txtLName.Text;
                customer.City = txtPlace.Text;
                customer.Street = txtStreet.Text;
                customer.PhoneNumber = txtPhoneNumber.Text;
                customer.BirthDate = dtpBirthDate.Value;

                dataContext.Customers.InsertOnSubmit(customer);
                dataContext.SubmitChanges();
            }
        }
    }
}
