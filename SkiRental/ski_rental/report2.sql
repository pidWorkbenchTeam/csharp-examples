SELECT ArticleNr, ArticleName, RentalStatus,
	   (SELECT COUNT(*) FROM ArticlesToCustomers
	    WHERE ArticlesToCustomers.ArticleNr = Articles.ArticleNr AND 
		  	  RentalDate BETWEEN '1.1.2010' AND '20.10.2020') AS RentalCount
FROM Articles