﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ski_rental
{
    public partial class Home : Form
    {
        DataClassesDataContext dataContext;
        ArticlesToCustomers atc_ = null;
        bool isNewRental = true;

        public Home()
        {
            InitializeComponent();
            atc_ = new ArticlesToCustomers();

            using (dataContext = new DataClassesDataContext())
            {
                var queryArticles = (from article in dataContext.Articles
                                     select new { ID = article.ArticleNr, Name = article.ArticleName}).ToList();
                queryArticles.Insert(0, new { ID = -1, Name = "---Select Article---" });
                articleList.DataSource = queryArticles;
                articleList.DisplayMember = "Name";
                articleList.ValueMember = "ID";

                var queryCustomers = (from customer in dataContext.Customers
                                      select new { ID = customer.CustomerID, Name = customer.FirstName + ' ' + customer.LastName}).ToList();
                queryCustomers.Insert(0, new { ID = -1, Name = "---Select Customer---" });
                customerList.DataSource = queryCustomers;
                customerList.DisplayMember = "Name";
                customerList.ValueMember = "ID";
            }
        }

        public Home(int atcId)
        {
            InitializeComponent();
            isNewRental = false;

            using (dataContext = new DataClassesDataContext())
            {
                atc_ = dataContext.ArticlesToCustomers.SingleOrDefault(atc => atc.ID == atcId);

                var queryArticles = (from article in dataContext.Articles
                                     select new { ID = article.ArticleNr, Name = article.ArticleName }).ToList();
                articleList.DataSource = queryArticles;
                articleList.DisplayMember = "Name";
                articleList.ValueMember = "ID";
                articleList.SelectedValue = atc_.ArticleNr;

                var queryCustomers = (from customer in dataContext.Customers
                                      select new { ID = customer.CustomerID, Name = customer.FirstName + ' ' + customer.LastName }).ToList();
                customerList.DataSource = queryCustomers;
                customerList.DisplayMember = "Name";
                customerList.ValueMember = "ID";
                customerList.SelectedValue = atc_.CustomerID;

                dtpRentUntil.Value = atc_.RentalDate;
            }
        }

        private void btnRent_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                if(!isNewRental)
                {
                    atc_.ArticleNr = (int)articleList.SelectedValue;
                    atc_.CustomerID = (int)customerList.SelectedValue;
                    atc_.RentalDate = dtpRentUntil.Value;

                    dataContext.ArticlesToCustomers.InsertOnSubmit(atc_);
                    dataContext.SubmitChanges();

                    MessageBox.Show("Der Artikel wurde modifiziert.");
                }
                else
                {
                    Articles article = dataContext.Articles.SingleOrDefault(a => a.ArticleNr == (int)articleList.SelectedValue);

                    if (!article.RentalStatus)
                    {
                        atc_.ArticleNr = (int)articleList.SelectedValue;
                        atc_.CustomerID = (int)customerList.SelectedValue;
                        atc_.RentalDate = dtpRentUntil.Value;

                        article.RentalStatus = true;
                        article.RentalCount = article.RentalCount + 1;

                        dataContext.ArticlesToCustomers.InsertOnSubmit(atc_);
                        dataContext.SubmitChanges();

                        MessageBox.Show("Der Artikel " + articleList.Text + " wurde von " + dtpRentUntil.Value.ToString() + " an " + customerList.Text + " verliehen");
                    }
                    else
                    {
                        MessageBox.Show("Der Artikel wurde bereits Verliehen.");
                    }
                }
            }
        }

        private void btnGiveBack_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                if(!isNewRental)
                {
                    Articles article = dataContext.Articles.SingleOrDefault(a => a.ArticleNr == (int)articleList.SelectedValue);
                    article.RentalStatus = false;
                    atc_.ReturnDate = dtpRentUntil.Value;
                    dataContext.ArticlesToCustomers.InsertOnSubmit(atc_);

                    dataContext.SubmitChanges();
                }
                else
                {
                    ArticlesToCustomers articleToCustomer = (from atc in dataContext.ArticlesToCustomers
                                                             where atc.ArticleNr == (int)articleList.SelectedValue && atc.CustomerID == (int)customerList.SelectedValue && atc.ReturnDate == null
                                                             select atc).SingleOrDefault();
                    if (articleToCustomer != null)
                    {
                        Articles article = dataContext.Articles.SingleOrDefault(a => a.ArticleNr == (int)articleList.SelectedValue);
                        article.RentalStatus = false;
                        articleToCustomer.ReturnDate = dtpRentUntil.Value;

                        dataContext.SubmitChanges();

                        MessageBox.Show("Der Artikel wurde zurück gegeben.");
                    }
                    else
                    {
                        MessageBox.Show("Der Artikel kann nicht zurückgegeben werden, da er noch nicht verliehen wurde");
                    }
                }
            }
        }

        private void btnOverview_Click(object sender, EventArgs e)
        {
            Form overviewForm = new Overview();
            overviewForm.Show();
        }

        private void btnCustomerOverview_Click(object sender, EventArgs e)
        {
            Form customerOverview = new CustomerOverview();
            customerOverview.Show();
        }
    }
}
