﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ski_rental
{
    public partial class LoginForm : Form
    {
        DataClassesDataContext dataContext;
        public int loginId_ = -1;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                var queryUser = (from user in dataContext.Users
                                 where user.Username == txtLogUsername.Text && user.Password == Encrypt.EncryptPasswordToHash(txtLogPassword.Text)
                                 select user).SingleOrDefault();

                if (queryUser != null)
                {
                    loginId_ = queryUser.UserID;
                    txtLogPassword.Clear();
                    txtLogUsername.Clear();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Password or username not found.");
                    txtLogUsername.Clear();
                    txtLogPassword.Clear();
                }              
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            Form registerForm = new Register();
            registerForm.Show();
        }
    }
}
