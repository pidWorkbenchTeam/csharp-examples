﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ski_rental
{
    public partial class RentalForm : Form
    {
        DataClassesDataContext dataContext;
        Users currentUser_;

        public RentalForm(int userId)
        {
            InitializeComponent();
            dataContext = new DataClassesDataContext();

            this.currentUser_ = dataContext.Users.SingleOrDefault(u => u.UserID == userId);
            this.Text = "Welcome " + currentUser_.Username;

            dgvRentals.DataSource = dataContext.Rentals;
            dgvRentals.Columns["returnDateDataGridViewTextBoxColumn"].DefaultCellStyle.NullValue = "RENTED";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form homeForm = new Home();
            homeForm.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                try
                {
                    int id = (int)dgvRentals.CurrentRow.Cells[0].Value;
                    ArticlesToCustomers atc = dataContext.ArticlesToCustomers.SingleOrDefault(a => a.ID == id);
                    dataContext.ArticlesToCustomers.DeleteOnSubmit(atc);
                    dataContext.SubmitChanges();
                    MessageBox.Show("ID: " + dgvRentals.CurrentRow.Cells[0].Value);
                    //dgvRentals.Rows.RemoveAt(dgvRentals.CurrentRow.Index);

                    //dgvRentals.Refresh();
                    MessageBox.Show("Der Verleih mit der ID " + (int)dgvRentals.CurrentRow.Cells[0].Value + " wurde erfolgreich gelöscht");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "error");
                    MessageBox.Show("Der Verleih existiert nicht");
                }
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            dataContext = new DataClassesDataContext();
            dgvRentals.DataSource = dataContext.Rentals;
        }

        private void btnRented_Click(object sender, EventArgs e)
        {
            dataContext = new DataClassesDataContext();
            var queryRented = from rental in dataContext.Rentals
                              where rental.ReturnDate == null
                              select rental;
            dgvRentals.DataSource = queryRented;
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Form homeForm = new Home((int)dgvRentals.CurrentRow.Cells[0].Value);
            homeForm.Show();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var query = from rental in dataContext.Rentals
                        where ((!string.IsNullOrWhiteSpace(tbArticleName.Text)) ? rental.ArticleName.Contains(tbArticleName.Text) : true) &&
                              ((!string.IsNullOrWhiteSpace(tbFirstName.Text)) ? rental.FirstName.Contains(tbFirstName.Text) : true) &&
                              ((!string.IsNullOrWhiteSpace(tbLastName.Text)) ? rental.LastName.Contains(tbLastName.Text) : true)
                        select rental;
            dgvRentals.DataSource = query;
            var query2 = from rental in dataContext.Rentals
                         select new Rentals
                         {
                             ReturnDate = rental.ReturnDate.Value
                             //ReturnDate = rental.ReturnDate.HasValue ? rental.ReturnDate.Value.ToString() : "Not yet returned"
                         };

        }
    }
}
