﻿namespace ski_rental
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOverview = new System.Windows.Forms.Button();
            this.articleList = new System.Windows.Forms.ComboBox();
            this.customerList = new System.Windows.Forms.ComboBox();
            this.btnRent = new System.Windows.Forms.Button();
            this.btnGiveBack = new System.Windows.Forms.Button();
            this.dtpRentUntil = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.Kunde = new System.Windows.Forms.Label();
            this.Artikel = new System.Windows.Forms.Label();
            this.btnCustomerOverview = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOverview
            // 
            this.btnOverview.Location = new System.Drawing.Point(12, 226);
            this.btnOverview.Name = "btnOverview";
            this.btnOverview.Size = new System.Drawing.Size(200, 23);
            this.btnOverview.TabIndex = 1;
            this.btnOverview.Text = "Artikel Übersicht";
            this.btnOverview.UseVisualStyleBackColor = true;
            this.btnOverview.Click += new System.EventHandler(this.btnOverview_Click);
            // 
            // articleList
            // 
            this.articleList.FormattingEnabled = true;
            this.articleList.Location = new System.Drawing.Point(12, 25);
            this.articleList.Name = "articleList";
            this.articleList.Size = new System.Drawing.Size(200, 21);
            this.articleList.TabIndex = 2;
            // 
            // customerList
            // 
            this.customerList.FormattingEnabled = true;
            this.customerList.Location = new System.Drawing.Point(12, 65);
            this.customerList.Name = "customerList";
            this.customerList.Size = new System.Drawing.Size(200, 21);
            this.customerList.TabIndex = 3;
            // 
            // btnRent
            // 
            this.btnRent.Location = new System.Drawing.Point(12, 131);
            this.btnRent.Name = "btnRent";
            this.btnRent.Size = new System.Drawing.Size(200, 23);
            this.btnRent.TabIndex = 4;
            this.btnRent.Text = "Verleihen";
            this.btnRent.UseVisualStyleBackColor = true;
            this.btnRent.Click += new System.EventHandler(this.btnRent_Click);
            // 
            // btnGiveBack
            // 
            this.btnGiveBack.Location = new System.Drawing.Point(12, 160);
            this.btnGiveBack.Name = "btnGiveBack";
            this.btnGiveBack.Size = new System.Drawing.Size(200, 23);
            this.btnGiveBack.TabIndex = 5;
            this.btnGiveBack.Text = "Zurück geben";
            this.btnGiveBack.UseVisualStyleBackColor = true;
            this.btnGiveBack.Click += new System.EventHandler(this.btnGiveBack_Click);
            // 
            // dtpRentUntil
            // 
            this.dtpRentUntil.Location = new System.Drawing.Point(12, 105);
            this.dtpRentUntil.Name = "dtpRentUntil";
            this.dtpRentUntil.Size = new System.Drawing.Size(200, 20);
            this.dtpRentUntil.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Verleihen bis";
            // 
            // Kunde
            // 
            this.Kunde.AutoSize = true;
            this.Kunde.Location = new System.Drawing.Point(9, 49);
            this.Kunde.Name = "Kunde";
            this.Kunde.Size = new System.Drawing.Size(38, 13);
            this.Kunde.TabIndex = 8;
            this.Kunde.Text = "Kunde";
            // 
            // Artikel
            // 
            this.Artikel.AutoSize = true;
            this.Artikel.Location = new System.Drawing.Point(9, 9);
            this.Artikel.Name = "Artikel";
            this.Artikel.Size = new System.Drawing.Size(36, 13);
            this.Artikel.TabIndex = 9;
            this.Artikel.Text = "Artikel";
            // 
            // btnCustomerOverview
            // 
            this.btnCustomerOverview.Location = new System.Drawing.Point(12, 197);
            this.btnCustomerOverview.Name = "btnCustomerOverview";
            this.btnCustomerOverview.Size = new System.Drawing.Size(200, 23);
            this.btnCustomerOverview.TabIndex = 10;
            this.btnCustomerOverview.Text = "Kunden Übersicht";
            this.btnCustomerOverview.UseVisualStyleBackColor = true;
            this.btnCustomerOverview.Click += new System.EventHandler(this.btnCustomerOverview_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 261);
            this.Controls.Add(this.btnCustomerOverview);
            this.Controls.Add(this.Artikel);
            this.Controls.Add(this.Kunde);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpRentUntil);
            this.Controls.Add(this.btnGiveBack);
            this.Controls.Add(this.btnRent);
            this.Controls.Add(this.customerList);
            this.Controls.Add(this.articleList);
            this.Controls.Add(this.btnOverview);
            this.Name = "Home";
            this.Text = "Home";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOverview;
        private System.Windows.Forms.ComboBox articleList;
        private System.Windows.Forms.ComboBox customerList;
        private System.Windows.Forms.Button btnRent;
        private System.Windows.Forms.Button btnGiveBack;
        private System.Windows.Forms.DateTimePicker dtpRentUntil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Kunde;
        private System.Windows.Forms.Label Artikel;
        private System.Windows.Forms.Button btnCustomerOverview;
    }
}