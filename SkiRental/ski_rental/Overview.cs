﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ski_rental
{
    public partial class Overview : Form
    {
        DataClassesDataContext dataContext;

        public Overview()
        {
            InitializeComponent();
        }

        private void btnSearchArticle_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                try
                {
                    var queryArticle = (from article in dataContext.Articles
                                        from category in dataContext.Categories
                                        where article.ArticleNr == Int32.Parse(txtArticleId.Text) && article.CategoryID == category.CategoryID
                                        select new { article.ArticleNr, article.ArticleName, article.PricePerDay, article.RentalStatus, category.CategoryName });

                    dgvDisplayArticle.DataSource = queryArticle;
                }
                catch
                {
                    MessageBox.Show("Bitte geben Sie eine Nummer ein");
                }
            }
        }
        private void btnShowAllArticles_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                var queryArticle = (from article in dataContext.Articles
                                    from category in dataContext.Categories
                                    select new { article.ArticleNr, article.ArticleName, article.PricePerDay, article.RentalStatus, category.CategoryName });

                dgvDisplayArticle.DataSource = queryArticle;
            }
        }

        private void btnDeleteArticle_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                try
                {                   
                    int articleNr = (int)dgvDisplayArticle.CurrentRow.Cells[0].Value;
                    Articles article = dataContext.Articles.SingleOrDefault(a => a.ArticleNr == articleNr);
                    dataContext.Articles.DeleteOnSubmit(article);
                    dataContext.SubmitChanges();

                    MessageBox.Show("Der Artikel mit der Artikel Nummer " + (int)dgvDisplayArticle.CurrentRow.Cells[0].Value + " wurde erfolgreich gelöscht");
                    dgvDisplayArticle.CurrentRow.Visible = false;
                }
                catch
                {
                    MessageBox.Show("Der Artikel Existiert nicht");
                }
            }
        }
    }
}
