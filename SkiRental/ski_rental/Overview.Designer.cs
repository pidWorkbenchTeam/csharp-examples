﻿namespace ski_rental
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtArticleId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchArticle = new System.Windows.Forms.Button();
            this.dgvDisplayArticle = new System.Windows.Forms.DataGridView();
            this.btnShowAllArticles = new System.Windows.Forms.Button();
            this.btnDeleteArticle = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayArticle)).BeginInit();
            this.SuspendLayout();
            // 
            // txtArticleId
            // 
            this.txtArticleId.Location = new System.Drawing.Point(12, 25);
            this.txtArticleId.Name = "txtArticleId";
            this.txtArticleId.Size = new System.Drawing.Size(156, 20);
            this.txtArticleId.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Artikel Nummer";
            // 
            // btnSearchArticle
            // 
            this.btnSearchArticle.Location = new System.Drawing.Point(174, 23);
            this.btnSearchArticle.Name = "btnSearchArticle";
            this.btnSearchArticle.Size = new System.Drawing.Size(118, 23);
            this.btnSearchArticle.TabIndex = 2;
            this.btnSearchArticle.Text = "Artikel Suchen";
            this.btnSearchArticle.UseVisualStyleBackColor = true;
            this.btnSearchArticle.Click += new System.EventHandler(this.btnSearchArticle_Click);
            // 
            // dgvDisplayArticle
            // 
            this.dgvDisplayArticle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDisplayArticle.Location = new System.Drawing.Point(12, 52);
            this.dgvDisplayArticle.Name = "dgvDisplayArticle";
            this.dgvDisplayArticle.Size = new System.Drawing.Size(605, 206);
            this.dgvDisplayArticle.TabIndex = 3;
            // 
            // btnShowAllArticles
            // 
            this.btnShowAllArticles.Location = new System.Drawing.Point(422, 23);
            this.btnShowAllArticles.Name = "btnShowAllArticles";
            this.btnShowAllArticles.Size = new System.Drawing.Size(195, 23);
            this.btnShowAllArticles.TabIndex = 4;
            this.btnShowAllArticles.Text = "Alle Artikel anzeigen";
            this.btnShowAllArticles.UseVisualStyleBackColor = true;
            this.btnShowAllArticles.Click += new System.EventHandler(this.btnShowAllArticles_Click);
            // 
            // btnDeleteArticle
            // 
            this.btnDeleteArticle.Location = new System.Drawing.Point(298, 23);
            this.btnDeleteArticle.Name = "btnDeleteArticle";
            this.btnDeleteArticle.Size = new System.Drawing.Size(118, 23);
            this.btnDeleteArticle.TabIndex = 5;
            this.btnDeleteArticle.Text = "Artikel Löschen";
            this.btnDeleteArticle.UseVisualStyleBackColor = true;
            this.btnDeleteArticle.Click += new System.EventHandler(this.btnDeleteArticle_Click);
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 270);
            this.Controls.Add(this.btnDeleteArticle);
            this.Controls.Add(this.btnShowAllArticles);
            this.Controls.Add(this.dgvDisplayArticle);
            this.Controls.Add(this.btnSearchArticle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtArticleId);
            this.Name = "Overview";
            this.Text = "Overview";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDisplayArticle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtArticleId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearchArticle;
        private System.Windows.Forms.DataGridView dgvDisplayArticle;
        private System.Windows.Forms.Button btnShowAllArticles;
        private System.Windows.Forms.Button btnDeleteArticle;
    }
}