SELECT Articles.ArticleNr, Articles.ArticleName, 
COUNT(ArticlesToCustomers.ArticleNr) as RentalCount, YEAR(RentalDate) as year, MONTH(RentalDate) as month 
FROM Articles, ArticlesToCustomers
WHERE ArticlesToCustomers.ArticleNr = Articles.ArticleNr
GROUP BY MONTH(RentalDate), YEAR(RentalDate), Articles.ArticleNr, Articles.ArticleName