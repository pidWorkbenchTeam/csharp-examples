SELECT Articles.ArticleNr, Articles.ArticleName, 
COUNT(ArticlesToCustomers.ArticleNr) as RentalCount, YEAR(RentalDate) as year, MONTH(RentalDate) as month 
FROM Articles LEFT OUTER JOIN
     ArticlesToCustomers ON Articles.ArticleNr = ArticlesToCustomers.ArticleNr
GROUP BY YEAR(RentalDate), MONTH(RentalDate), Articles.ArticleNr, Articles.ArticleName