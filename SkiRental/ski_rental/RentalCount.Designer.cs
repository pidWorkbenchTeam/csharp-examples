﻿namespace ski_rental
{
    partial class RentalCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvRentalCount = new System.Windows.Forms.DataGridView();
            this.articleNrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.articleNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pricePerDayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentalCountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rentalStatusDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.categoryIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoriesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsRentalCount = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalCount)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRentalCount
            // 
            this.dgvRentalCount.AutoGenerateColumns = false;
            this.dgvRentalCount.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRentalCount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRentalCount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.articleNrDataGridViewTextBoxColumn,
            this.articleNameDataGridViewTextBoxColumn,
            this.pricePerDayDataGridViewTextBoxColumn,
            this.rentalCountDataGridViewTextBoxColumn,
            this.rentalStatusDataGridViewCheckBoxColumn,
            this.categoryIDDataGridViewTextBoxColumn,
            this.categoriesDataGridViewTextBoxColumn});
            this.dgvRentalCount.DataSource = this.bdsRentalCount;
            this.dgvRentalCount.Location = new System.Drawing.Point(12, 214);
            this.dgvRentalCount.Name = "dgvRentalCount";
            this.dgvRentalCount.RowTemplate.Height = 24;
            this.dgvRentalCount.Size = new System.Drawing.Size(613, 150);
            this.dgvRentalCount.TabIndex = 0;
            // 
            // articleNrDataGridViewTextBoxColumn
            // 
            this.articleNrDataGridViewTextBoxColumn.DataPropertyName = "ArticleNr";
            this.articleNrDataGridViewTextBoxColumn.HeaderText = "ArticleNr";
            this.articleNrDataGridViewTextBoxColumn.Name = "articleNrDataGridViewTextBoxColumn";
            // 
            // articleNameDataGridViewTextBoxColumn
            // 
            this.articleNameDataGridViewTextBoxColumn.DataPropertyName = "ArticleName";
            this.articleNameDataGridViewTextBoxColumn.HeaderText = "ArticleName";
            this.articleNameDataGridViewTextBoxColumn.Name = "articleNameDataGridViewTextBoxColumn";
            // 
            // pricePerDayDataGridViewTextBoxColumn
            // 
            this.pricePerDayDataGridViewTextBoxColumn.DataPropertyName = "PricePerDay";
            this.pricePerDayDataGridViewTextBoxColumn.HeaderText = "PricePerDay";
            this.pricePerDayDataGridViewTextBoxColumn.Name = "pricePerDayDataGridViewTextBoxColumn";
            // 
            // rentalCountDataGridViewTextBoxColumn
            // 
            this.rentalCountDataGridViewTextBoxColumn.DataPropertyName = "RentalCount";
            this.rentalCountDataGridViewTextBoxColumn.HeaderText = "RentalCount";
            this.rentalCountDataGridViewTextBoxColumn.Name = "rentalCountDataGridViewTextBoxColumn";
            // 
            // rentalStatusDataGridViewCheckBoxColumn
            // 
            this.rentalStatusDataGridViewCheckBoxColumn.DataPropertyName = "RentalStatus";
            this.rentalStatusDataGridViewCheckBoxColumn.HeaderText = "RentalStatus";
            this.rentalStatusDataGridViewCheckBoxColumn.Name = "rentalStatusDataGridViewCheckBoxColumn";
            // 
            // categoryIDDataGridViewTextBoxColumn
            // 
            this.categoryIDDataGridViewTextBoxColumn.DataPropertyName = "CategoryID";
            this.categoryIDDataGridViewTextBoxColumn.HeaderText = "CategoryID";
            this.categoryIDDataGridViewTextBoxColumn.Name = "categoryIDDataGridViewTextBoxColumn";
            // 
            // categoriesDataGridViewTextBoxColumn
            // 
            this.categoriesDataGridViewTextBoxColumn.DataPropertyName = "Categories";
            this.categoriesDataGridViewTextBoxColumn.HeaderText = "Categories";
            this.categoriesDataGridViewTextBoxColumn.Name = "categoriesDataGridViewTextBoxColumn";
            // 
            // bdsRentalCount
            // 
            this.bdsRentalCount.DataSource = typeof(ski_rental.Articles);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(102, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RentalCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 413);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvRentalCount);
            this.Name = "RentalCount";
            this.Text = "RentalCount";
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRentalCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn articleNrDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn articleNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pricePerDayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rentalCountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rentalStatusDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoriesDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bdsRentalCount;
        private System.Windows.Forms.Button button1;
    }
}