﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ski_rental
{
    public partial class Register : Form
    {
        DataClassesDataContext dataContext;
        Users user;

        public Register()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            using (dataContext = new DataClassesDataContext())
            {
                user = new Users();
                user.Username = txtRegUsername.Text;
                user.Password = Encrypt.EncryptPasswordToHash(txtRegPassword.Text);

                if (txtRegPasswordConfirm.Text == txtRegPassword.Text)
                {
                    try
                    {
                        dataContext.Users.InsertOnSubmit(user);
                        dataContext.SubmitChanges();

                        MessageBox.Show("User created successfully, try to login now");
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error");
                    }
                }
                else
                {
                    MessageBox.Show("Password confirm doesn't match");
                }
            }
        }
    }
}
