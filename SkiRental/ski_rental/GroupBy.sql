SELECT ArticleNr, ArticleName, 
(SELECT Count(ArticleNr) as RentalCount, YEAR(RentalDate) as year, MONTH(RentalDate) as month FROM ArticlesToCustomers 
GROUP BY YEAR(RentalDate), MONTH(RentalDate))
FROM Articles