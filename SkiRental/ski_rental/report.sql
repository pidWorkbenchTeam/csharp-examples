SELECT DISTINCT ArticleNr, ArticleName, RentalStatus,
	    (SELECT COUNT(ArticleNr) FROM RentalViewOld RvI
	     WHERE RvI.ArticleNr = RvO.ArticleNr AND 
		  	   RentalDate BETWEEN '1.1.2010' AND '20.10.2020') AS RentalCount
FROM RentalViewOld RvO