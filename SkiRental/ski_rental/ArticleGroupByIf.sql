SELECT TOP (100) PERCENT dbo.Articles.ArticleNr, 
						 dbo.Articles.ArticleName, 
						 COUNT(dbo.ArticlesToCustomers.ArticleNr) AS Counter, 
						 YEAR(dbo.ArticlesToCustomers.RentalDate) AS Year, 
						 MONTH(dbo.ArticlesToCustomers.RentalDate) AS Month,
						 IIF( (SELECT COUNT(*) FROM dbo.ArticlesToCustomers 
							   WHERE ArticleNr = 1 AND ArticlesToCustomers.ReturnDate IS NULL) = 1, 'true', 'false' ) AS IsRented
FROM            dbo.Articles INNER JOIN
                         dbo.ArticlesToCustomers ON dbo.Articles.ArticleNr = dbo.ArticlesToCustomers.ArticleNr
GROUP BY YEAR(dbo.ArticlesToCustomers.RentalDate), MONTH(dbo.ArticlesToCustomers.RentalDate), dbo.Articles.ArticleNr, dbo.Articles.ArticleName
ORDER BY Year DESC, Month DESC