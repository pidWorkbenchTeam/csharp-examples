﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace LinqTestForm
{
    public partial class CustomerForm : Form
    {
        // Use the following connection string.
        Northwind db = new Northwind(@"Data Source=DESKTOP-NML6L7C\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=True");

        public CustomerForm()
        {
            InitializeComponent();
        }

        private void CustomerForm_Load(object sender, EventArgs e)
        {
            var allCustomers = (from cust in db.Customers
                                select cust);
            CustomerBindingSource.DataSource = allCustomers;
            CustomerDataGridView.DataSource = CustomerBindingSource;
            CustomerBindingNavigator.BindingSource = CustomerBindingSource;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            if (CustomerBindingNavigator.Validate()) MessageBox.Show("Validation Success!", "Validation...", MessageBoxButtons.OK);
            try { db.SubmitChanges(); }
            catch (System.Data.SqlClient.SqlException ex)
            {
                MessageBox.Show(ex.GetType().FullName + "\n" + ex.Message, "Error...", MessageBoxButtons.OK);
            }
        }
    }
}
