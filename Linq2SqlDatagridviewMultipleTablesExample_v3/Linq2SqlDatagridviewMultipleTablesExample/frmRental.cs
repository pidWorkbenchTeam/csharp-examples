﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq2SqlDatagridviewMultipleTablesExample
{
    public partial class frmRental : Form
    {
        public frmRental()
        {
            InitializeComponent();
            DataClassesDataContext dataContext = new DataClassesDataContext();
            dgvRentals.DataSource = dataContext.GetTable<RentalView>();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DataClassesDataContext dataContext = new DataClassesDataContext();
            try
            {
                dataContext.InsertRental(0, 0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            dgvRentals.DataSource = dataContext.GetTable<RentalView>();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataClassesDataContext dataContext = new DataClassesDataContext();
            try
            {
                dataContext.DeleteRental((int)dgvRentals.CurrentRow.Cells[6].Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            dgvRentals.DataSource = dataContext.GetTable<RentalView>();
        }
    }
}
