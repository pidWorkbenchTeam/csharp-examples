﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace BookRentalLinq
{
    public partial class RentalForm : Form
    {
        // Use the following connection string.
        BookRentalDb db = new BookRentalDb(@"Data Source=DESKTOP-NML6L7C\SQLEXPRESS;Initial Catalog=BookRentalDb;Integrated Security=True");

        public RentalForm()
        {
            InitializeComponent();
        }

        private void RentalForm_Load(object sender, EventArgs e)
        {
            var allRentals = (from rent in db.Rentals
                            select new
                            {
                                rent.Person.FirstName,
                                rent.Person.LastName,
                                rent.Book.Title,
                                rent.IssueDate,
                                rent.ReturnDate
                            }).ToList();
            RentalBindingSource.DataSource = allRentals;
            CustomerDataGridView.DataSource = RentalBindingSource;
            RentalBindingNavigator.BindingSource = RentalBindingSource;
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            //Rental rent = db.Rentals.FirstOrDefault(rent => rent.Id.Equals(e.ColumnIndex);
            if (RentalBindingNavigator.Validate()) MessageBox.Show("Validation Success!", "Validation...", MessageBoxButtons.OK);
            try { db.SubmitChanges(); }
            catch (System.Data.SqlClient.SqlException ex)
            {
                MessageBox.Show(ex.GetType().FullName + "\n" + ex.Message, "Error...", MessageBoxButtons.OK);
            }
        }
    }
}
