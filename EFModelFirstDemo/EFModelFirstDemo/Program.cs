﻿using System;
using System.Linq;

namespace EFModelFirstDemo
{
    class Program
    {
        static void Main(string[] args)
        {            
            using (var db = new ModelFirstDemoDBContainer())
            {
                // Create and save a new Student
                Console.Write("Enter a first name for a new Student: ");
                var firstName = Console.ReadLine();
                Console.Write("Enter a last name for a new Student: ");
                var lastName = Console.ReadLine();
                var student = new Student
                {
                    Id = 1,
                    FirstName = firstName,
                    LastName = lastName,
                    EnrollmentDate = DateTime.Now
                };
                db.Students.Add(student);
                db.SaveChanges();
                var query = from b in db.Students
                            orderby b.LastName
                            select b;
                Console.WriteLine("All student in the database:");
                foreach (var item in query)
                {
                    Console.WriteLine("{0}, {1}, {2:dd.MM.yyyy HH:mm}", item.FirstName, item.LastName, item.EnrollmentDate);
                }
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }
    }
}
