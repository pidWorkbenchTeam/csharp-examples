﻿namespace Linq2SqlBookRentalFull
{
    partial class NewRental
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.dgvNewRentals = new System.Windows.Forms.DataGridView();
            this.dgvNewRentalBooks = new System.Windows.Forms.DataGridView();
            this.btnSelectPerson = new System.Windows.Forms.Button();
            this.btnSaveRentals = new System.Windows.Forms.Button();
            this.btnSearchRentals = new System.Windows.Forms.Button();
            this.bdsNewRentalBooks = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsNewRentals = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issueDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewRentals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewRentalBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNewRentalBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNewRentals)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddBook
            // 
            this.btnAddBook.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddBook.Location = new System.Drawing.Point(776, 104);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(88, 32);
            this.btnAddBook.TabIndex = 14;
            this.btnAddBook.Text = "Add Book";
            this.btnAddBook.UseVisualStyleBackColor = true;
            // 
            // dgvNewRentals
            // 
            this.dgvNewRentals.AllowUserToAddRows = false;
            this.dgvNewRentals.AllowUserToDeleteRows = false;
            this.dgvNewRentals.AutoGenerateColumns = false;
            this.dgvNewRentals.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNewRentals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewRentals.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.issueDateDataGridViewTextBoxColumn,
            this.personIdDataGridViewTextBoxColumn,
            this.personsDataGridViewTextBoxColumn});
            this.dgvNewRentals.DataSource = this.bdsNewRentals;
            this.dgvNewRentals.Location = new System.Drawing.Point(12, 12);
            this.dgvNewRentals.Name = "dgvNewRentals";
            this.dgvNewRentals.ReadOnly = true;
            this.dgvNewRentals.RowTemplate.Height = 24;
            this.dgvNewRentals.Size = new System.Drawing.Size(311, 168);
            this.dgvNewRentals.TabIndex = 13;
            // 
            // dgvNewRentalBooks
            // 
            this.dgvNewRentalBooks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvNewRentalBooks.AutoGenerateColumns = false;
            this.dgvNewRentalBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvNewRentalBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewRentalBooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.isbnDataGridViewTextBoxColumn});
            this.dgvNewRentalBooks.DataSource = this.bdsNewRentalBooks;
            this.dgvNewRentalBooks.Location = new System.Drawing.Point(12, 191);
            this.dgvNewRentalBooks.Name = "dgvNewRentalBooks";
            this.dgvNewRentalBooks.RowTemplate.Height = 24;
            this.dgvNewRentalBooks.Size = new System.Drawing.Size(311, 218);
            this.dgvNewRentalBooks.TabIndex = 12;
            // 
            // btnSelectPerson
            // 
            this.btnSelectPerson.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectPerson.Location = new System.Drawing.Point(776, 51);
            this.btnSelectPerson.Name = "btnSelectPerson";
            this.btnSelectPerson.Size = new System.Drawing.Size(88, 42);
            this.btnSelectPerson.TabIndex = 10;
            this.btnSelectPerson.Text = "Select Person";
            this.btnSelectPerson.UseVisualStyleBackColor = true;
            // 
            // btnSaveRentals
            // 
            this.btnSaveRentals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveRentals.Location = new System.Drawing.Point(762, 142);
            this.btnSaveRentals.Name = "btnSaveRentals";
            this.btnSaveRentals.Size = new System.Drawing.Size(88, 267);
            this.btnSaveRentals.TabIndex = 9;
            this.btnSaveRentals.Text = "Save";
            this.btnSaveRentals.UseVisualStyleBackColor = true;
            // 
            // btnSearchRentals
            // 
            this.btnSearchRentals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchRentals.Location = new System.Drawing.Point(776, 12);
            this.btnSearchRentals.Name = "btnSearchRentals";
            this.btnSearchRentals.Size = new System.Drawing.Size(88, 33);
            this.btnSearchRentals.TabIndex = 8;
            this.btnSearchRentals.Text = "Search";
            this.btnSearchRentals.UseVisualStyleBackColor = true;
            // 
            // bdsNewRentalBooks
            // 
            this.bdsNewRentalBooks.DataSource = typeof(Linq2SqlBookRentalFull.Books);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            // 
            // isbnDataGridViewTextBoxColumn
            // 
            this.isbnDataGridViewTextBoxColumn.DataPropertyName = "isbn";
            this.isbnDataGridViewTextBoxColumn.HeaderText = "isbn";
            this.isbnDataGridViewTextBoxColumn.Name = "isbnDataGridViewTextBoxColumn";
            // 
            // bdsNewRentals
            // 
            this.bdsNewRentals.DataSource = typeof(Linq2SqlBookRentalFull.Rentals);
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // issueDateDataGridViewTextBoxColumn
            // 
            this.issueDateDataGridViewTextBoxColumn.DataPropertyName = "issueDate";
            this.issueDateDataGridViewTextBoxColumn.HeaderText = "issueDate";
            this.issueDateDataGridViewTextBoxColumn.Name = "issueDateDataGridViewTextBoxColumn";
            this.issueDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // personIdDataGridViewTextBoxColumn
            // 
            this.personIdDataGridViewTextBoxColumn.DataPropertyName = "personId";
            this.personIdDataGridViewTextBoxColumn.HeaderText = "personId";
            this.personIdDataGridViewTextBoxColumn.Name = "personIdDataGridViewTextBoxColumn";
            this.personIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // personsDataGridViewTextBoxColumn
            // 
            this.personsDataGridViewTextBoxColumn.DataPropertyName = "Persons";
            this.personsDataGridViewTextBoxColumn.HeaderText = "Persons";
            this.personsDataGridViewTextBoxColumn.Name = "personsDataGridViewTextBoxColumn";
            this.personsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // NewRental
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 421);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.dgvNewRentals);
            this.Controls.Add(this.dgvNewRentalBooks);
            this.Controls.Add(this.btnSelectPerson);
            this.Controls.Add(this.btnSaveRentals);
            this.Controls.Add(this.btnSearchRentals);
            this.Name = "NewRental";
            this.Text = "NewRental";
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewRentals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewRentalBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNewRentalBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsNewRentals)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.DataGridView dgvNewRentals;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn issueDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personsDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bdsNewRentals;
        private System.Windows.Forms.DataGridView dgvNewRentalBooks;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbnDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bdsNewRentalBooks;
        private System.Windows.Forms.Button btnSelectPerson;
        private System.Windows.Forms.Button btnSaveRentals;
        private System.Windows.Forms.Button btnSearchRentals;
    }
}