﻿namespace Linq2SqlBookRentalFull
{
    partial class FrmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpRentals = new System.Windows.Forms.TabPage();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbLastname = new System.Windows.Forms.TextBox();
            this.tbFirstname = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblLastname = new System.Windows.Forms.Label();
            this.lblFirstname = new System.Windows.Forms.Label();
            this.dgvRentalBooks = new System.Windows.Forms.DataGridView();
            this.bookIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isbnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsRentalBooks = new System.Windows.Forms.BindingSource(this.components);
            this.dgvRental = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.issueDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsRentals = new System.Windows.Forms.BindingSource(this.components);
            this.btnNewRental = new System.Windows.Forms.Button();
            this.btnSaveRentals = new System.Windows.Forms.Button();
            this.btnSearchRentals = new System.Windows.Forms.Button();
            this.tpBooks = new System.Windows.Forms.TabPage();
            this.btnSaveBooks = new System.Windows.Forms.Button();
            this.btnSelectBooks = new System.Windows.Forms.Button();
            this.btnSearchBooks = new System.Windows.Forms.Button();
            this.tbBooksIsbn = new System.Windows.Forms.TextBox();
            this.tbBooksTitle = new System.Windows.Forms.TextBox();
            this.dgvBooks = new System.Windows.Forms.DataGridView();
            this.tbcBooksId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcBooksTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcBooksIsbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsBooks = new System.Windows.Forms.BindingSource(this.components);
            this.tpPersons = new System.Windows.Forms.TabPage();
            this.btnSelectPersons = new System.Windows.Forms.Button();
            this.tbPersonsLastname = new System.Windows.Forms.TextBox();
            this.tbPersonsFirstname = new System.Windows.Forms.TextBox();
            this.btnSearchPersons = new System.Windows.Forms.Button();
            this.btnSavePersons = new System.Windows.Forms.Button();
            this.dgvPersons = new System.Windows.Forms.DataGridView();
            this.tbcPersonsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcPersonsFirstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcPersonsLastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcPersonsAddressId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcPersonsAddresses = new System.Windows.Forms.DataGridViewButtonColumn();
            this.bdsPersons = new System.Windows.Forms.BindingSource(this.components);
            this.tpAddresses = new System.Windows.Forms.TabPage();
            this.btnSelectAddresses = new System.Windows.Forms.Button();
            this.tbAddressesCountry = new System.Windows.Forms.TextBox();
            this.tbAddressesZip = new System.Windows.Forms.TextBox();
            this.tbAddressesNumber = new System.Windows.Forms.TextBox();
            this.tbAddressesStreet = new System.Windows.Forms.TextBox();
            this.btnSearchAddresses = new System.Windows.Forms.Button();
            this.btnSaveAddresses = new System.Windows.Forms.Button();
            this.dgvAddresses = new System.Windows.Forms.DataGridView();
            this.tbcAddressesId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcAddressesStreet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcAddressesNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcAddressesZip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcAddressesCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsAddresses = new System.Windows.Forms.BindingSource(this.components);
            this.tpBookByPerson = new System.Windows.Forms.TabPage();
            this.tpPersonByBook = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dgvPersonByBook = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bdsRentalView = new System.Windows.Forms.BindingSource(this.components);
            this.tpRentedBooks = new System.Windows.Forms.TabPage();
            this.btnShowRentedBooks = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvRentedBooks = new System.Windows.Forms.DataGridView();
            this.tbcRentalsFirstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsLastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsIssueDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsReturnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsIsbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsRentalId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsPersonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbcRentalsBookId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tcMain.SuspendLayout();
            this.tpRentals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentalBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRental)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentals)).BeginInit();
            this.tpBooks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBooks)).BeginInit();
            this.tpPersons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPersons)).BeginInit();
            this.tpAddresses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsAddresses)).BeginInit();
            this.tpPersonByBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonByBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalView)).BeginInit();
            this.tpRentedBooks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentedBooks)).BeginInit();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tpRentals);
            this.tcMain.Controls.Add(this.tpBooks);
            this.tcMain.Controls.Add(this.tpPersons);
            this.tcMain.Controls.Add(this.tpAddresses);
            this.tcMain.Controls.Add(this.tpBookByPerson);
            this.tcMain.Controls.Add(this.tpPersonByBook);
            this.tcMain.Controls.Add(this.tpRentedBooks);
            this.tcMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tcMain.Location = new System.Drawing.Point(0, 0);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(867, 388);
            this.tcMain.TabIndex = 0;
            // 
            // tpRentals
            // 
            this.tpRentals.Controls.Add(this.tbAddress);
            this.tpRentals.Controls.Add(this.tbLastname);
            this.tpRentals.Controls.Add(this.tbFirstname);
            this.tpRentals.Controls.Add(this.lblAddress);
            this.tpRentals.Controls.Add(this.lblLastname);
            this.tpRentals.Controls.Add(this.lblFirstname);
            this.tpRentals.Controls.Add(this.dgvRentalBooks);
            this.tpRentals.Controls.Add(this.dgvRental);
            this.tpRentals.Controls.Add(this.btnNewRental);
            this.tpRentals.Controls.Add(this.btnSaveRentals);
            this.tpRentals.Controls.Add(this.btnSearchRentals);
            this.tpRentals.Location = new System.Drawing.Point(4, 25);
            this.tpRentals.Name = "tpRentals";
            this.tpRentals.Padding = new System.Windows.Forms.Padding(3);
            this.tpRentals.Size = new System.Drawing.Size(859, 359);
            this.tpRentals.TabIndex = 3;
            this.tpRentals.Text = "Rentals";
            this.tpRentals.UseVisualStyleBackColor = true;
            this.tpRentals.Enter += new System.EventHandler(this.tpRentals_Enter);
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(464, 100);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.ReadOnly = true;
            this.tbAddress.Size = new System.Drawing.Size(222, 22);
            this.tbAddress.TabIndex = 13;
            // 
            // tbLastname
            // 
            this.tbLastname.Location = new System.Drawing.Point(589, 42);
            this.tbLastname.Name = "tbLastname";
            this.tbLastname.Size = new System.Drawing.Size(97, 22);
            this.tbLastname.TabIndex = 12;
            // 
            // tbFirstname
            // 
            this.tbFirstname.Location = new System.Drawing.Point(464, 42);
            this.tbFirstname.Name = "tbFirstname";
            this.tbFirstname.Size = new System.Drawing.Size(97, 22);
            this.tbFirstname.TabIndex = 11;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(461, 76);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(60, 17);
            this.lblAddress.TabIndex = 10;
            this.lblAddress.Text = "Address";
            // 
            // lblLastname
            // 
            this.lblLastname.AutoSize = true;
            this.lblLastname.Location = new System.Drawing.Point(586, 18);
            this.lblLastname.Name = "lblLastname";
            this.lblLastname.Size = new System.Drawing.Size(70, 17);
            this.lblLastname.TabIndex = 9;
            this.lblLastname.Text = "Lastname";
            // 
            // lblFirstname
            // 
            this.lblFirstname.AutoSize = true;
            this.lblFirstname.Location = new System.Drawing.Point(461, 18);
            this.lblFirstname.Name = "lblFirstname";
            this.lblFirstname.Size = new System.Drawing.Size(70, 17);
            this.lblFirstname.TabIndex = 8;
            this.lblFirstname.Text = "Firstname";
            // 
            // dgvRentalBooks
            // 
            this.dgvRentalBooks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRentalBooks.AutoGenerateColumns = false;
            this.dgvRentalBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRentalBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRentalBooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bookIdDataGridViewTextBoxColumn,
            this.isbnDataGridViewTextBoxColumn,
            this.titleDataGridViewTextBoxColumn,
            this.returnDateDataGridViewTextBoxColumn});
            this.dgvRentalBooks.DataSource = this.bdsRentalBooks;
            this.dgvRentalBooks.Location = new System.Drawing.Point(448, 139);
            this.dgvRentalBooks.Name = "dgvRentalBooks";
            this.dgvRentalBooks.RowTemplate.Height = 24;
            this.dgvRentalBooks.Size = new System.Drawing.Size(311, 217);
            this.dgvRentalBooks.TabIndex = 5;
            // 
            // bookIdDataGridViewTextBoxColumn
            // 
            this.bookIdDataGridViewTextBoxColumn.DataPropertyName = "BookId";
            this.bookIdDataGridViewTextBoxColumn.HeaderText = "BookId";
            this.bookIdDataGridViewTextBoxColumn.Name = "bookIdDataGridViewTextBoxColumn";
            this.bookIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // isbnDataGridViewTextBoxColumn
            // 
            this.isbnDataGridViewTextBoxColumn.DataPropertyName = "Isbn";
            this.isbnDataGridViewTextBoxColumn.HeaderText = "Isbn";
            this.isbnDataGridViewTextBoxColumn.Name = "isbnDataGridViewTextBoxColumn";
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            // 
            // returnDateDataGridViewTextBoxColumn
            // 
            this.returnDateDataGridViewTextBoxColumn.DataPropertyName = "ReturnDate";
            this.returnDateDataGridViewTextBoxColumn.HeaderText = "ReturnDate";
            this.returnDateDataGridViewTextBoxColumn.Name = "returnDateDataGridViewTextBoxColumn";
            // 
            // bdsRentalBooks
            // 
            this.bdsRentalBooks.DataSource = typeof(Linq2SqlBookRentalFull.FrmMain.BookWithDate);
            // 
            // dgvRental
            // 
            this.dgvRental.AutoGenerateColumns = false;
            this.dgvRental.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRental.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRental.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.personsDataGridViewTextBoxColumn,
            this.issueDateDataGridViewTextBoxColumn,
            this.personIdDataGridViewTextBoxColumn});
            this.dgvRental.DataSource = this.bdsRentals;
            this.dgvRental.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvRental.Location = new System.Drawing.Point(3, 3);
            this.dgvRental.Name = "dgvRental";
            this.dgvRental.RowTemplate.Height = 24;
            this.dgvRental.Size = new System.Drawing.Size(439, 353);
            this.dgvRental.TabIndex = 4;
            this.dgvRental.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRental_CellEnter);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // personsDataGridViewTextBoxColumn
            // 
            this.personsDataGridViewTextBoxColumn.DataPropertyName = "Persons";
            this.personsDataGridViewTextBoxColumn.HeaderText = "Persons";
            this.personsDataGridViewTextBoxColumn.Name = "personsDataGridViewTextBoxColumn";
            // 
            // issueDateDataGridViewTextBoxColumn
            // 
            this.issueDateDataGridViewTextBoxColumn.DataPropertyName = "issueDate";
            this.issueDateDataGridViewTextBoxColumn.HeaderText = "issueDate";
            this.issueDateDataGridViewTextBoxColumn.Name = "issueDateDataGridViewTextBoxColumn";
            // 
            // personIdDataGridViewTextBoxColumn
            // 
            this.personIdDataGridViewTextBoxColumn.DataPropertyName = "personId";
            this.personIdDataGridViewTextBoxColumn.HeaderText = "personId";
            this.personIdDataGridViewTextBoxColumn.Name = "personIdDataGridViewTextBoxColumn";
            this.personIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // bdsRentals
            // 
            this.bdsRentals.DataSource = typeof(Linq2SqlBookRentalFull.Rentals);
            // 
            // btnNewRental
            // 
            this.btnNewRental.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNewRental.Location = new System.Drawing.Point(765, 42);
            this.btnNewRental.Name = "btnNewRental";
            this.btnNewRental.Size = new System.Drawing.Size(88, 33);
            this.btnNewRental.TabIndex = 3;
            this.btnNewRental.Text = "New";
            this.btnNewRental.UseVisualStyleBackColor = true;
            this.btnNewRental.Click += new System.EventHandler(this.btnNewRental_Click);
            // 
            // btnSaveRentals
            // 
            this.btnSaveRentals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveRentals.Location = new System.Drawing.Point(765, 81);
            this.btnSaveRentals.Name = "btnSaveRentals";
            this.btnSaveRentals.Size = new System.Drawing.Size(88, 275);
            this.btnSaveRentals.TabIndex = 2;
            this.btnSaveRentals.Text = "Save";
            this.btnSaveRentals.UseVisualStyleBackColor = true;
            this.btnSaveRentals.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSearchRentals
            // 
            this.btnSearchRentals.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchRentals.Location = new System.Drawing.Point(765, 3);
            this.btnSearchRentals.Name = "btnSearchRentals";
            this.btnSearchRentals.Size = new System.Drawing.Size(88, 33);
            this.btnSearchRentals.TabIndex = 1;
            this.btnSearchRentals.Text = "Search";
            this.btnSearchRentals.UseVisualStyleBackColor = true;
            // 
            // tpBooks
            // 
            this.tpBooks.Controls.Add(this.btnSaveBooks);
            this.tpBooks.Controls.Add(this.btnSelectBooks);
            this.tpBooks.Controls.Add(this.btnSearchBooks);
            this.tpBooks.Controls.Add(this.tbBooksIsbn);
            this.tpBooks.Controls.Add(this.tbBooksTitle);
            this.tpBooks.Controls.Add(this.dgvBooks);
            this.tpBooks.Location = new System.Drawing.Point(4, 25);
            this.tpBooks.Name = "tpBooks";
            this.tpBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tpBooks.Size = new System.Drawing.Size(859, 359);
            this.tpBooks.TabIndex = 2;
            this.tpBooks.Text = "Books";
            this.tpBooks.UseVisualStyleBackColor = true;
            this.tpBooks.Enter += new System.EventHandler(this.tpBooks_Enter);
            this.tpBooks.Leave += new System.EventHandler(this.tpBooks_Leave);
            // 
            // btnSaveBooks
            // 
            this.btnSaveBooks.Location = new System.Drawing.Point(765, 46);
            this.btnSaveBooks.Name = "btnSaveBooks";
            this.btnSaveBooks.Size = new System.Drawing.Size(88, 313);
            this.btnSaveBooks.TabIndex = 5;
            this.btnSaveBooks.Text = "Save";
            this.btnSaveBooks.UseVisualStyleBackColor = true;
            this.btnSaveBooks.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSelectBooks
            // 
            this.btnSelectBooks.Location = new System.Drawing.Point(765, 45);
            this.btnSelectBooks.Name = "btnSelectBooks";
            this.btnSelectBooks.Size = new System.Drawing.Size(88, 33);
            this.btnSelectBooks.TabIndex = 4;
            this.btnSelectBooks.Text = "Select";
            this.btnSelectBooks.UseVisualStyleBackColor = true;
            this.btnSelectBooks.Visible = false;
            this.btnSelectBooks.Click += new System.EventHandler(this.btnSelectBooks_Click);
            // 
            // btnSearchBooks
            // 
            this.btnSearchBooks.Location = new System.Drawing.Point(765, 6);
            this.btnSearchBooks.Name = "btnSearchBooks";
            this.btnSearchBooks.Size = new System.Drawing.Size(88, 34);
            this.btnSearchBooks.TabIndex = 3;
            this.btnSearchBooks.Text = "Search";
            this.btnSearchBooks.UseVisualStyleBackColor = true;
            this.btnSearchBooks.Click += new System.EventHandler(this.btnSearchBooks_Click);
            // 
            // tbBooksIsbn
            // 
            this.tbBooksIsbn.Location = new System.Drawing.Point(397, 17);
            this.tbBooksIsbn.Name = "tbBooksIsbn";
            this.tbBooksIsbn.Size = new System.Drawing.Size(334, 22);
            this.tbBooksIsbn.TabIndex = 2;
            // 
            // tbBooksTitle
            // 
            this.tbBooksTitle.Location = new System.Drawing.Point(57, 17);
            this.tbBooksTitle.Name = "tbBooksTitle";
            this.tbBooksTitle.Size = new System.Drawing.Size(334, 22);
            this.tbBooksTitle.TabIndex = 1;
            // 
            // dgvBooks
            // 
            this.dgvBooks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvBooks.AutoGenerateColumns = false;
            this.dgvBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tbcBooksId,
            this.tbcBooksTitle,
            this.tbcBooksIsbn});
            this.dgvBooks.DataSource = this.bdsBooks;
            this.dgvBooks.Location = new System.Drawing.Point(3, 45);
            this.dgvBooks.Name = "dgvBooks";
            this.dgvBooks.RowTemplate.Height = 24;
            this.dgvBooks.Size = new System.Drawing.Size(756, 317);
            this.dgvBooks.TabIndex = 0;
            this.dgvBooks.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvBooks_DefaultValuesNeeded);
            // 
            // tbcBooksId
            // 
            this.tbcBooksId.DataPropertyName = "id";
            this.tbcBooksId.HeaderText = "id";
            this.tbcBooksId.Name = "tbcBooksId";
            this.tbcBooksId.Visible = false;
            // 
            // tbcBooksTitle
            // 
            this.tbcBooksTitle.DataPropertyName = "title";
            this.tbcBooksTitle.HeaderText = "title";
            this.tbcBooksTitle.Name = "tbcBooksTitle";
            // 
            // tbcBooksIsbn
            // 
            this.tbcBooksIsbn.DataPropertyName = "isbn";
            this.tbcBooksIsbn.HeaderText = "isbn";
            this.tbcBooksIsbn.Name = "tbcBooksIsbn";
            // 
            // bdsBooks
            // 
            this.bdsBooks.DataSource = typeof(Linq2SqlBookRentalFull.Books);
            // 
            // tpPersons
            // 
            this.tpPersons.Controls.Add(this.btnSelectPersons);
            this.tpPersons.Controls.Add(this.tbPersonsLastname);
            this.tpPersons.Controls.Add(this.tbPersonsFirstname);
            this.tpPersons.Controls.Add(this.btnSearchPersons);
            this.tpPersons.Controls.Add(this.btnSavePersons);
            this.tpPersons.Controls.Add(this.dgvPersons);
            this.tpPersons.Location = new System.Drawing.Point(4, 25);
            this.tpPersons.Name = "tpPersons";
            this.tpPersons.Padding = new System.Windows.Forms.Padding(3);
            this.tpPersons.Size = new System.Drawing.Size(859, 359);
            this.tpPersons.TabIndex = 1;
            this.tpPersons.Text = "Persons";
            this.tpPersons.UseVisualStyleBackColor = true;
            this.tpPersons.Enter += new System.EventHandler(this.tpPersons_Enter);
            this.tpPersons.Leave += new System.EventHandler(this.tpPersons_Leave);
            // 
            // btnSelectPersons
            // 
            this.btnSelectPersons.Location = new System.Drawing.Point(765, 45);
            this.btnSelectPersons.Name = "btnSelectPersons";
            this.btnSelectPersons.Size = new System.Drawing.Size(86, 33);
            this.btnSelectPersons.TabIndex = 6;
            this.btnSelectPersons.Text = "Select";
            this.btnSelectPersons.UseVisualStyleBackColor = true;
            this.btnSelectPersons.Visible = false;
            this.btnSelectPersons.Click += new System.EventHandler(this.btnSelectPersons_Click);
            // 
            // tbPersonsLastname
            // 
            this.tbPersonsLastname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPersonsLastname.Location = new System.Drawing.Point(279, 11);
            this.tbPersonsLastname.Multiline = true;
            this.tbPersonsLastname.Name = "tbPersonsLastname";
            this.tbPersonsLastname.Size = new System.Drawing.Size(232, 28);
            this.tbPersonsLastname.TabIndex = 5;
            // 
            // tbPersonsFirstname
            // 
            this.tbPersonsFirstname.Location = new System.Drawing.Point(41, 11);
            this.tbPersonsFirstname.Multiline = true;
            this.tbPersonsFirstname.Name = "tbPersonsFirstname";
            this.tbPersonsFirstname.Size = new System.Drawing.Size(232, 28);
            this.tbPersonsFirstname.TabIndex = 4;
            // 
            // btnSearchPersons
            // 
            this.btnSearchPersons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchPersons.Location = new System.Drawing.Point(765, 6);
            this.btnSearchPersons.Name = "btnSearchPersons";
            this.btnSearchPersons.Size = new System.Drawing.Size(88, 33);
            this.btnSearchPersons.TabIndex = 2;
            this.btnSearchPersons.Text = "Search";
            this.btnSearchPersons.UseVisualStyleBackColor = true;
            this.btnSearchPersons.Click += new System.EventHandler(this.btnSearchPersons_Click);
            // 
            // btnSavePersons
            // 
            this.btnSavePersons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSavePersons.Location = new System.Drawing.Point(765, 45);
            this.btnSavePersons.Name = "btnSavePersons";
            this.btnSavePersons.Size = new System.Drawing.Size(88, 317);
            this.btnSavePersons.TabIndex = 1;
            this.btnSavePersons.Text = "Save";
            this.btnSavePersons.UseVisualStyleBackColor = true;
            this.btnSavePersons.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgvPersons
            // 
            this.dgvPersons.AllowUserToDeleteRows = false;
            this.dgvPersons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPersons.AutoGenerateColumns = false;
            this.dgvPersons.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPersons.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersons.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tbcPersonsId,
            this.tbcPersonsFirstname,
            this.tbcPersonsLastname,
            this.tbcPersonsAddressId,
            this.tbcPersonsAddresses});
            this.dgvPersons.DataSource = this.bdsPersons;
            this.dgvPersons.Location = new System.Drawing.Point(3, 45);
            this.dgvPersons.Name = "dgvPersons";
            this.dgvPersons.RowTemplate.Height = 24;
            this.dgvPersons.Size = new System.Drawing.Size(756, 317);
            this.dgvPersons.TabIndex = 0;
            this.dgvPersons.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPersons_CellContentClick);
            this.dgvPersons.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvPersons_DefaultValuesNeeded);
            // 
            // tbcPersonsId
            // 
            this.tbcPersonsId.DataPropertyName = "id";
            this.tbcPersonsId.HeaderText = "id";
            this.tbcPersonsId.Name = "tbcPersonsId";
            this.tbcPersonsId.Visible = false;
            // 
            // tbcPersonsFirstname
            // 
            this.tbcPersonsFirstname.DataPropertyName = "firstname";
            this.tbcPersonsFirstname.HeaderText = "firstname";
            this.tbcPersonsFirstname.Name = "tbcPersonsFirstname";
            // 
            // tbcPersonsLastname
            // 
            this.tbcPersonsLastname.DataPropertyName = "lastname";
            this.tbcPersonsLastname.HeaderText = "lastname";
            this.tbcPersonsLastname.Name = "tbcPersonsLastname";
            // 
            // tbcPersonsAddressId
            // 
            this.tbcPersonsAddressId.DataPropertyName = "addressId";
            this.tbcPersonsAddressId.HeaderText = "addressId";
            this.tbcPersonsAddressId.Name = "tbcPersonsAddressId";
            this.tbcPersonsAddressId.Visible = false;
            // 
            // tbcPersonsAddresses
            // 
            this.tbcPersonsAddresses.DataPropertyName = "Addresses";
            this.tbcPersonsAddresses.HeaderText = "Addresses";
            this.tbcPersonsAddresses.Name = "tbcPersonsAddresses";
            this.tbcPersonsAddresses.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.tbcPersonsAddresses.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // bdsPersons
            // 
            this.bdsPersons.DataSource = typeof(Linq2SqlBookRentalFull.Persons);
            // 
            // tpAddresses
            // 
            this.tpAddresses.Controls.Add(this.btnSelectAddresses);
            this.tpAddresses.Controls.Add(this.tbAddressesCountry);
            this.tpAddresses.Controls.Add(this.tbAddressesZip);
            this.tpAddresses.Controls.Add(this.tbAddressesNumber);
            this.tpAddresses.Controls.Add(this.tbAddressesStreet);
            this.tpAddresses.Controls.Add(this.btnSearchAddresses);
            this.tpAddresses.Controls.Add(this.btnSaveAddresses);
            this.tpAddresses.Controls.Add(this.dgvAddresses);
            this.tpAddresses.Location = new System.Drawing.Point(4, 25);
            this.tpAddresses.Name = "tpAddresses";
            this.tpAddresses.Padding = new System.Windows.Forms.Padding(3);
            this.tpAddresses.Size = new System.Drawing.Size(859, 359);
            this.tpAddresses.TabIndex = 0;
            this.tpAddresses.Text = "Addresses";
            this.tpAddresses.UseVisualStyleBackColor = true;
            this.tpAddresses.Enter += new System.EventHandler(this.tpAddresses_Enter);
            this.tpAddresses.Leave += new System.EventHandler(this.tpAddresses_Leave);
            // 
            // btnSelectAddresses
            // 
            this.btnSelectAddresses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectAddresses.Location = new System.Drawing.Point(765, 45);
            this.btnSelectAddresses.Name = "btnSelectAddresses";
            this.btnSelectAddresses.Size = new System.Drawing.Size(88, 33);
            this.btnSelectAddresses.TabIndex = 7;
            this.btnSelectAddresses.Text = "Select";
            this.btnSelectAddresses.UseVisualStyleBackColor = true;
            this.btnSelectAddresses.Visible = false;
            this.btnSelectAddresses.Click += new System.EventHandler(this.btnSelectAddresses_Click);
            // 
            // tbAddressesCountry
            // 
            this.tbAddressesCountry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddressesCountry.Location = new System.Drawing.Point(580, 11);
            this.tbAddressesCountry.Multiline = true;
            this.tbAddressesCountry.Name = "tbAddressesCountry";
            this.tbAddressesCountry.Size = new System.Drawing.Size(178, 28);
            this.tbAddressesCountry.TabIndex = 6;
            // 
            // tbAddressesZip
            // 
            this.tbAddressesZip.Location = new System.Drawing.Point(402, 11);
            this.tbAddressesZip.Multiline = true;
            this.tbAddressesZip.Name = "tbAddressesZip";
            this.tbAddressesZip.Size = new System.Drawing.Size(172, 28);
            this.tbAddressesZip.TabIndex = 5;
            // 
            // tbAddressesNumber
            // 
            this.tbAddressesNumber.Location = new System.Drawing.Point(225, 11);
            this.tbAddressesNumber.Multiline = true;
            this.tbAddressesNumber.Name = "tbAddressesNumber";
            this.tbAddressesNumber.Size = new System.Drawing.Size(171, 28);
            this.tbAddressesNumber.TabIndex = 4;
            // 
            // tbAddressesStreet
            // 
            this.tbAddressesStreet.Location = new System.Drawing.Point(40, 11);
            this.tbAddressesStreet.Multiline = true;
            this.tbAddressesStreet.Name = "tbAddressesStreet";
            this.tbAddressesStreet.Size = new System.Drawing.Size(179, 28);
            this.tbAddressesStreet.TabIndex = 3;
            // 
            // btnSearchAddresses
            // 
            this.btnSearchAddresses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearchAddresses.Location = new System.Drawing.Point(765, 6);
            this.btnSearchAddresses.Name = "btnSearchAddresses";
            this.btnSearchAddresses.Size = new System.Drawing.Size(88, 33);
            this.btnSearchAddresses.TabIndex = 2;
            this.btnSearchAddresses.Text = "Search";
            this.btnSearchAddresses.UseVisualStyleBackColor = true;
            this.btnSearchAddresses.Click += new System.EventHandler(this.btnSearchAddresses_Click);
            // 
            // btnSaveAddresses
            // 
            this.btnSaveAddresses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAddresses.Location = new System.Drawing.Point(765, 45);
            this.btnSaveAddresses.Name = "btnSaveAddresses";
            this.btnSaveAddresses.Size = new System.Drawing.Size(88, 317);
            this.btnSaveAddresses.TabIndex = 1;
            this.btnSaveAddresses.Text = "Save";
            this.btnSaveAddresses.UseVisualStyleBackColor = true;
            this.btnSaveAddresses.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dgvAddresses
            // 
            this.dgvAddresses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAddresses.AutoGenerateColumns = false;
            this.dgvAddresses.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAddresses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddresses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tbcAddressesId,
            this.tbcAddressesStreet,
            this.tbcAddressesNumber,
            this.tbcAddressesZip,
            this.tbcAddressesCountry});
            this.dgvAddresses.DataSource = this.bdsAddresses;
            this.dgvAddresses.Location = new System.Drawing.Point(0, 45);
            this.dgvAddresses.Name = "dgvAddresses";
            this.dgvAddresses.RowTemplate.Height = 24;
            this.dgvAddresses.Size = new System.Drawing.Size(759, 317);
            this.dgvAddresses.TabIndex = 0;
            this.dgvAddresses.DefaultValuesNeeded += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvAddresses_DefaultValuesNeeded);
            // 
            // tbcAddressesId
            // 
            this.tbcAddressesId.DataPropertyName = "id";
            this.tbcAddressesId.HeaderText = "id";
            this.tbcAddressesId.Name = "tbcAddressesId";
            this.tbcAddressesId.Visible = false;
            // 
            // tbcAddressesStreet
            // 
            this.tbcAddressesStreet.DataPropertyName = "street";
            this.tbcAddressesStreet.HeaderText = "street";
            this.tbcAddressesStreet.Name = "tbcAddressesStreet";
            // 
            // tbcAddressesNumber
            // 
            this.tbcAddressesNumber.DataPropertyName = "number";
            this.tbcAddressesNumber.HeaderText = "number";
            this.tbcAddressesNumber.Name = "tbcAddressesNumber";
            // 
            // tbcAddressesZip
            // 
            this.tbcAddressesZip.DataPropertyName = "zip";
            this.tbcAddressesZip.HeaderText = "zip";
            this.tbcAddressesZip.Name = "tbcAddressesZip";
            // 
            // tbcAddressesCountry
            // 
            this.tbcAddressesCountry.DataPropertyName = "country";
            this.tbcAddressesCountry.HeaderText = "country";
            this.tbcAddressesCountry.Name = "tbcAddressesCountry";
            // 
            // bdsAddresses
            // 
            this.bdsAddresses.DataSource = typeof(Linq2SqlBookRentalFull.Addresses);
            // 
            // tpBookByPerson
            // 
            this.tpBookByPerson.Location = new System.Drawing.Point(4, 25);
            this.tpBookByPerson.Name = "tpBookByPerson";
            this.tpBookByPerson.Padding = new System.Windows.Forms.Padding(3);
            this.tpBookByPerson.Size = new System.Drawing.Size(859, 359);
            this.tpBookByPerson.TabIndex = 4;
            this.tpBookByPerson.Text = "BookByPerson";
            this.tpBookByPerson.UseVisualStyleBackColor = true;
            // 
            // tpPersonByBook
            // 
            this.tpPersonByBook.Controls.Add(this.label1);
            this.tpPersonByBook.Controls.Add(this.textBox1);
            this.tpPersonByBook.Controls.Add(this.dgvPersonByBook);
            this.tpPersonByBook.Location = new System.Drawing.Point(4, 25);
            this.tpPersonByBook.Name = "tpPersonByBook";
            this.tpPersonByBook.Padding = new System.Windows.Forms.Padding(3);
            this.tpPersonByBook.Size = new System.Drawing.Size(859, 359);
            this.tpPersonByBook.TabIndex = 5;
            this.tpPersonByBook.Text = "PersonByBook";
            this.tpPersonByBook.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(799, 278);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(759, 317);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 3;
            // 
            // dgvPersonByBook
            // 
            this.dgvPersonByBook.AllowUserToAddRows = false;
            this.dgvPersonByBook.AllowUserToDeleteRows = false;
            this.dgvPersonByBook.AutoGenerateColumns = false;
            this.dgvPersonByBook.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPersonByBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPersonByBook.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.dgvPersonByBook.DataSource = this.bdsRentalView;
            this.dgvPersonByBook.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvPersonByBook.Location = new System.Drawing.Point(3, 3);
            this.dgvPersonByBook.Name = "dgvPersonByBook";
            this.dgvPersonByBook.ReadOnly = true;
            this.dgvPersonByBook.RowTemplate.Height = 24;
            this.dgvPersonByBook.Size = new System.Drawing.Size(754, 353);
            this.dgvPersonByBook.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "firstname";
            this.dataGridViewTextBoxColumn1.HeaderText = "firstname";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "lastname";
            this.dataGridViewTextBoxColumn2.HeaderText = "lastname";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "issueDate";
            this.dataGridViewTextBoxColumn3.HeaderText = "issueDate";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "returnDate";
            this.dataGridViewTextBoxColumn4.HeaderText = "returnDate";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "title";
            this.dataGridViewTextBoxColumn5.HeaderText = "title";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "isbn";
            this.dataGridViewTextBoxColumn6.HeaderText = "isbn";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "rentalId";
            this.dataGridViewTextBoxColumn7.HeaderText = "rentalId";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "personId";
            this.dataGridViewTextBoxColumn8.HeaderText = "personId";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "bookId";
            this.dataGridViewTextBoxColumn9.HeaderText = "bookId";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // bdsRentalView
            // 
            this.bdsRentalView.DataSource = typeof(Linq2SqlBookRentalFull.RentalView);
            // 
            // tpRentedBooks
            // 
            this.tpRentedBooks.Controls.Add(this.btnShowRentedBooks);
            this.tpRentedBooks.Controls.Add(this.button1);
            this.tpRentedBooks.Controls.Add(this.dgvRentedBooks);
            this.tpRentedBooks.Location = new System.Drawing.Point(4, 25);
            this.tpRentedBooks.Name = "tpRentedBooks";
            this.tpRentedBooks.Padding = new System.Windows.Forms.Padding(3);
            this.tpRentedBooks.Size = new System.Drawing.Size(859, 359);
            this.tpRentedBooks.TabIndex = 6;
            this.tpRentedBooks.Text = "RentedBooks";
            this.tpRentedBooks.UseVisualStyleBackColor = true;
            this.tpRentedBooks.Enter += new System.EventHandler(this.tpRentedBooks_Enter);
            // 
            // btnShowRentedBooks
            // 
            this.btnShowRentedBooks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowRentedBooks.Location = new System.Drawing.Point(763, 6);
            this.btnShowRentedBooks.Name = "btnShowRentedBooks";
            this.btnShowRentedBooks.Size = new System.Drawing.Size(88, 111);
            this.btnShowRentedBooks.TabIndex = 3;
            this.btnShowRentedBooks.Text = "Show";
            this.btnShowRentedBooks.UseVisualStyleBackColor = true;
            this.btnShowRentedBooks.Click += new System.EventHandler(this.btnShowRentedBooks_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(763, 123);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 233);
            this.button1.TabIndex = 2;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dgvRentedBooks
            // 
            this.dgvRentedBooks.AllowUserToAddRows = false;
            this.dgvRentedBooks.AllowUserToDeleteRows = false;
            this.dgvRentedBooks.AutoGenerateColumns = false;
            this.dgvRentedBooks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRentedBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRentedBooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tbcRentalsFirstname,
            this.tbcRentalsLastname,
            this.tbcRentalsIssueDate,
            this.tbcRentalsReturnDate,
            this.tbcRentalsTitle,
            this.tbcRentalsIsbn,
            this.tbcRentalsRentalId,
            this.tbcRentalsPersonId,
            this.tbcRentalsBookId});
            this.dgvRentedBooks.DataSource = this.bdsRentalView;
            this.dgvRentedBooks.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgvRentedBooks.Location = new System.Drawing.Point(3, 3);
            this.dgvRentedBooks.Name = "dgvRentedBooks";
            this.dgvRentedBooks.ReadOnly = true;
            this.dgvRentedBooks.RowTemplate.Height = 24;
            this.dgvRentedBooks.Size = new System.Drawing.Size(754, 353);
            this.dgvRentedBooks.TabIndex = 1;
            // 
            // tbcRentalsFirstname
            // 
            this.tbcRentalsFirstname.DataPropertyName = "firstname";
            this.tbcRentalsFirstname.HeaderText = "firstname";
            this.tbcRentalsFirstname.Name = "tbcRentalsFirstname";
            this.tbcRentalsFirstname.ReadOnly = true;
            // 
            // tbcRentalsLastname
            // 
            this.tbcRentalsLastname.DataPropertyName = "lastname";
            this.tbcRentalsLastname.HeaderText = "lastname";
            this.tbcRentalsLastname.Name = "tbcRentalsLastname";
            this.tbcRentalsLastname.ReadOnly = true;
            // 
            // tbcRentalsIssueDate
            // 
            this.tbcRentalsIssueDate.DataPropertyName = "issueDate";
            this.tbcRentalsIssueDate.HeaderText = "issueDate";
            this.tbcRentalsIssueDate.Name = "tbcRentalsIssueDate";
            this.tbcRentalsIssueDate.ReadOnly = true;
            // 
            // tbcRentalsReturnDate
            // 
            this.tbcRentalsReturnDate.DataPropertyName = "returnDate";
            this.tbcRentalsReturnDate.HeaderText = "returnDate";
            this.tbcRentalsReturnDate.Name = "tbcRentalsReturnDate";
            this.tbcRentalsReturnDate.ReadOnly = true;
            this.tbcRentalsReturnDate.Visible = false;
            // 
            // tbcRentalsTitle
            // 
            this.tbcRentalsTitle.DataPropertyName = "title";
            this.tbcRentalsTitle.HeaderText = "title";
            this.tbcRentalsTitle.Name = "tbcRentalsTitle";
            this.tbcRentalsTitle.ReadOnly = true;
            // 
            // tbcRentalsIsbn
            // 
            this.tbcRentalsIsbn.DataPropertyName = "isbn";
            this.tbcRentalsIsbn.HeaderText = "isbn";
            this.tbcRentalsIsbn.Name = "tbcRentalsIsbn";
            this.tbcRentalsIsbn.ReadOnly = true;
            // 
            // tbcRentalsRentalId
            // 
            this.tbcRentalsRentalId.DataPropertyName = "rentalId";
            this.tbcRentalsRentalId.HeaderText = "rentalId";
            this.tbcRentalsRentalId.Name = "tbcRentalsRentalId";
            this.tbcRentalsRentalId.ReadOnly = true;
            this.tbcRentalsRentalId.Visible = false;
            // 
            // tbcRentalsPersonId
            // 
            this.tbcRentalsPersonId.DataPropertyName = "personId";
            this.tbcRentalsPersonId.HeaderText = "personId";
            this.tbcRentalsPersonId.Name = "tbcRentalsPersonId";
            this.tbcRentalsPersonId.ReadOnly = true;
            this.tbcRentalsPersonId.Visible = false;
            // 
            // tbcRentalsBookId
            // 
            this.tbcRentalsBookId.DataPropertyName = "bookId";
            this.tbcRentalsBookId.HeaderText = "bookId";
            this.tbcRentalsBookId.Name = "tbcRentalsBookId";
            this.tbcRentalsBookId.ReadOnly = true;
            this.tbcRentalsBookId.Visible = false;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 388);
            this.Controls.Add(this.tcMain);
            this.Name = "FrmMain";
            this.Text = "BookRental";
            this.tcMain.ResumeLayout(false);
            this.tpRentals.ResumeLayout(false);
            this.tpRentals.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentalBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRental)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentals)).EndInit();
            this.tpBooks.ResumeLayout(false);
            this.tpBooks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBooks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsBooks)).EndInit();
            this.tpPersons.ResumeLayout(false);
            this.tpPersons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsPersons)).EndInit();
            this.tpAddresses.ResumeLayout(false);
            this.tpAddresses.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsAddresses)).EndInit();
            this.tpPersonByBook.ResumeLayout(false);
            this.tpPersonByBook.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPersonByBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsRentalView)).EndInit();
            this.tpRentedBooks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRentedBooks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpAddresses;
        private System.Windows.Forms.TabPage tpPersons;
        private System.Windows.Forms.TabPage tpBooks;
        private System.Windows.Forms.TabPage tpRentals;
        private System.Windows.Forms.DataGridView dgvAddresses;
        private System.Windows.Forms.BindingSource bdsAddresses;
        private System.Windows.Forms.DataGridView dgvPersons;
        private System.Windows.Forms.BindingSource bdsPersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcAddressesId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcAddressesStreet;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcAddressesNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcAddressesZip;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcAddressesCountry;
        private System.Windows.Forms.Button btnSearchAddresses;
        private System.Windows.Forms.Button btnSaveAddresses;
        private System.Windows.Forms.TextBox tbAddressesCountry;
        private System.Windows.Forms.TextBox tbAddressesZip;
        private System.Windows.Forms.TextBox tbAddressesNumber;
        private System.Windows.Forms.TextBox tbAddressesStreet;
        private System.Windows.Forms.Button btnSearchPersons;
        private System.Windows.Forms.Button btnSavePersons;
        private System.Windows.Forms.TextBox tbPersonsLastname;
        private System.Windows.Forms.TextBox tbPersonsFirstname;
        public System.Windows.Forms.Button btnSelectAddresses;
        private System.Windows.Forms.DataGridView dgvBooks;
        private System.Windows.Forms.BindingSource bdsBooks;
        private System.Windows.Forms.Button btnSaveBooks;
        private System.Windows.Forms.Button btnSelectBooks;
        private System.Windows.Forms.Button btnSearchBooks;
        private System.Windows.Forms.TextBox tbBooksIsbn;
        private System.Windows.Forms.TextBox tbBooksTitle;
        private System.Windows.Forms.Button btnSaveRentals;
        private System.Windows.Forms.Button btnSearchRentals;
        private System.Windows.Forms.BindingSource bdsRentalView;
        private System.Windows.Forms.Button btnSelectPersons;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcPersonsId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcPersonsFirstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcPersonsLastname;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcPersonsAddressId;
        private System.Windows.Forms.DataGridViewButtonColumn tbcPersonsAddresses;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcBooksId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcBooksTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcBooksIsbn;
        private System.Windows.Forms.Button btnNewRental;
        private System.Windows.Forms.DataGridView dgvRental;
        private System.Windows.Forms.BindingSource bdsRentals;
        private System.Windows.Forms.DataGridView dgvRentalBooks;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isbnDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn returnDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource bdsRentalBooks;
        private System.Windows.Forms.TabPage tpBookByPerson;
        private System.Windows.Forms.TabPage tpPersonByBook;
        private System.Windows.Forms.TabPage tpRentedBooks;
        private System.Windows.Forms.Button btnShowRentedBooks;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvRentedBooks;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsFirstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsLastname;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsIssueDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsReturnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsIsbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsRentalId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsPersonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tbcRentalsBookId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dgvPersonByBook;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.TextBox tbLastname;
        private System.Windows.Forms.TextBox tbFirstname;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblLastname;
        private System.Windows.Forms.Label lblFirstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn issueDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn personIdDataGridViewTextBoxColumn;
    }
}

