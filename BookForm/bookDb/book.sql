﻿CREATE TABLE [dbo].[book]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [title] NVARCHAR(MAX) NOT NULL, 
    [description] NTEXT NULL, 
    [isbn] CHAR(17) NOT NULL UNIQUE, 
    [price] INT NOT NULL 
)