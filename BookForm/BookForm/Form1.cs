﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BookForm
{
    public partial class Form1 : Form
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-89J2OD1\SQLEXPRESS;Initial Catalog=Buchverleih;Integrated Security=True");
        String strSelect;
        String strInsert;
        String strUpdate;
        String strDelete;
        SqlCommand sqlSelectCmd;
        SqlCommand sqlInsertCmd;
        SqlCommand sqlUpdateCmd;
        SqlCommand sqlDeleteCmd;
        DataTable dataTable;
        SqlDataAdapter dataAdapter;

        public Form1()
        {
            InitializeComponent();
            dataTable = new DataTable();
         // SELECT
            strSelect = "SELECT * from book";
            sqlSelectCmd = new SqlCommand(strSelect, sqlCon);
            dataAdapter = new SqlDataAdapter(sqlSelectCmd);
         // INSERT
            strInsert = "INSERT INTO book (id, title, description, isbn, price) " +
                        "VALUES(@id, @title, @description, @isbn, @price)";
            sqlInsertCmd = new SqlCommand(strInsert, sqlCon);
            dataAdapter.InsertCommand = sqlInsertCmd;
            SqlParameter sqlInsertParam = dataAdapter.InsertCommand.Parameters.Add("@id", SqlDbType.Int);
            sqlInsertParam.SourceColumn = "id";
            sqlInsertParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.InsertCommand.Parameters.Add("@title", SqlDbType.NVarChar, -1, "title");
            dataAdapter.InsertCommand.Parameters.Add("@description", SqlDbType.NText, 0, "description");
            dataAdapter.InsertCommand.Parameters.Add("@isbn", SqlDbType.Char, 17, "isbn");
            dataAdapter.InsertCommand.Parameters.Add("@price", SqlDbType.Int, 4, "price");
            // UPDATE
            strUpdate = "UPDATE book " +
                        "SET title = @title, description = @description, isbn = @isbn, price = @price " +
                        "WHERE id = @id";
            sqlUpdateCmd = new SqlCommand(strUpdate, sqlCon);
            dataAdapter.UpdateCommand = sqlUpdateCmd;

            SqlParameter sqlUpdateParam = dataAdapter.UpdateCommand.Parameters.Add("@id", SqlDbType.Int);
            sqlUpdateParam.SourceColumn = "id";
            sqlUpdateParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.UpdateCommand.Parameters.Add("@title", SqlDbType.NVarChar, -1, "title");
            dataAdapter.UpdateCommand.Parameters.Add("@description", SqlDbType.NText, 0, "description");
            dataAdapter.UpdateCommand.Parameters.Add("@isbn", SqlDbType.Char, 17, "isbn");
            dataAdapter.UpdateCommand.Parameters.Add("@price", SqlDbType.Int, 4, "price");
            // DELETE
            strDelete = "DELETE FROM book " +
                        "WHERE id = @id";
            sqlDeleteCmd = new SqlCommand(strDelete, sqlCon);
            dataAdapter.DeleteCommand = sqlDeleteCmd;
            dataAdapter.UpdateCommand.Parameters.Add("@id", SqlDbType.Int, 4, "id");

            //SqlParameter sqlDeleteParam = dataAdapter.DeleteCommand.Parameters.Add("@id", SqlDbType.Int);
            //sqlDeleteParam.SourceColumn = "id";
            //sqlDeleteParam.SourceVersion = DataRowVersion.Original;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            displayView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            displayView();
        }

        private void displayView()
        {
            dataTable.Clear();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable newDT = dataTable.GetChanges();
                if (newDT != null)
                {
                    dataAdapter.Update(newDT);
                }
                MessageBox.Show("Information Updated", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
