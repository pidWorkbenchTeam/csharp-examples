﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BookForm
{
    public partial class Form1 : Form
    {
        SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-NML6L7C\SQLEXPRESS;Initial Catalog=BookDb;Integrated Security=True");
        SqlCommand sqlCmd;
        SqlParameter sqlParam;
        DataTable dataTable;
        SqlDataAdapter dataAdapter;

        public Form1()
        {
            InitializeComponent();
            dataTable = new DataTable();
         
         // SELECT
            dataAdapter = new SqlDataAdapter("SELECT * from book", sqlCon);

         // INSERT
            sqlCmd = new SqlCommand("INSERT INTO book (id, title, description, isbn, price) " +
                                    "VALUES(@id, @title, @description, @isbn, @price)", sqlCon);
            sqlCmd.Parameters.Add("@id", SqlDbType.Int, 4, "id");
            sqlCmd.Parameters.Add("@title", SqlDbType.NVarChar, -1, "title");
            sqlCmd.Parameters.Add("@description", SqlDbType.NText, 0, "description");
            sqlCmd.Parameters.Add("@isbn", SqlDbType.Char, 17, "isbn");
            sqlCmd.Parameters.Add("@price", SqlDbType.Int, 4, "price");
            dataAdapter.InsertCommand = sqlCmd;

         // UPDATE
            sqlCmd = new SqlCommand("UPDATE book " +
                                    "SET title = @title, description = @description, isbn = @isbn, price = @price " +
                                     "WHERE id = @id", sqlCon);
            sqlCmd.Parameters.Add("@title", SqlDbType.NVarChar, -1, "title");
            sqlCmd.Parameters.Add("@description", SqlDbType.NText, 0, "description");
            sqlCmd.Parameters.Add("@isbn", SqlDbType.Char, 17, "isbn");
            sqlCmd.Parameters.Add("@price", SqlDbType.Int, 4, "price");
            sqlParam = sqlCmd.Parameters.Add("@id", SqlDbType.Int, 8, "id");
            sqlParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.UpdateCommand = sqlCmd;

         // DELETE
            sqlCmd = new SqlCommand("DELETE FROM book " +
                                    "WHERE id = @id", sqlCon);
            sqlParam = sqlCmd.Parameters.Add("@id", SqlDbType.Int, 8, "id");
            sqlParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.DeleteCommand = sqlCmd;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            displayView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            displayView();
        }

        private void displayView()
        {
            dataTable.Clear();
            dataAdapter.Fill(dataTable);
            dataGridView1.DataSource = dataTable;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable newDT = dataTable.GetChanges();
                if (newDT != null)
                {
                    dataAdapter.Update(newDT);
                }
                MessageBox.Show("Information Updated", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
