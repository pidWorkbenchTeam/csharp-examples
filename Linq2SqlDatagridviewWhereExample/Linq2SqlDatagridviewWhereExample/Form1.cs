﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq2SqlDatagridviewWhereExample
{
    public partial class BooksForm : Form
    {
        DataClassesDataContext dataContext = new DataClassesDataContext();

        public BooksForm()
        {
            InitializeComponent();
            dataGridViewBooks.DataSource = dataContext.GetTable<Book>();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dataContext.SubmitChanges();
                MessageBox.Show("Data was saved successfully!", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var query = from books in dataContext.GetTable<Book>() select books;

            if (!string.IsNullOrWhiteSpace(txtBoxTitle.Text))
            {
                //query = query.Where(books => books.title.StartsWith(txtBoxTitle.Text) );
                query = query.Where(books => books.title.Contains(txtBoxTitle.Text));
            }

            if (!string.IsNullOrWhiteSpace(txtBoxISBN.Text))
            {
                //query = query.Where(books => books.isbn.StartsWith(txtBoxISBN.Text));
                query = query.Where(books => books.isbn.Contains(txtBoxISBN.Text));
            }

            dataGridViewBooks.DataSource = query;
        }

        private void dataGridViewBooks_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[0].Value = e.Row.Index + 1;
        }

        //private void dataGridViewBooks_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        //{
        //    MessageBox.Show(e.Row.Index.ToString());
        //    e.Row.Cells[0].Value = e.Row.Index;
        //}
    }
}
