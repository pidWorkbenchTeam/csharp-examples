﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BookRentalApp
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="BookRentalAppDb")]
	public partial class DataClassesDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnCreated();
    partial void InsertBooks(Books instance);
    partial void UpdateBooks(Books instance);
    partial void DeleteBooks(Books instance);
    partial void InsertRentals(Rentals instance);
    partial void UpdateRentals(Rentals instance);
    partial void DeleteRentals(Rentals instance);
    partial void InsertRentalBook(RentalBook instance);
    partial void UpdateRentalBook(RentalBook instance);
    partial void DeleteRentalBook(RentalBook instance);
    partial void InsertPersons(Persons instance);
    partial void UpdatePersons(Persons instance);
    partial void DeletePersons(Persons instance);
    partial void InsertAddresses(Addresses instance);
    partial void UpdateAddresses(Addresses instance);
    partial void DeleteAddresses(Addresses instance);
    #endregion
		
		public DataClassesDataContext() : 
				base(global::BookRentalAdvanced.Properties.Settings.Default.BookRentalAppDbConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClassesDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClassesDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClassesDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClassesDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Books> Books
		{
			get
			{
				return this.GetTable<Books>();
			}
		}
		
		public System.Data.Linq.Table<Rentals> Rentals
		{
			get
			{
				return this.GetTable<Rentals>();
			}
		}
		
		public System.Data.Linq.Table<RentalBook> RentalBook
		{
			get
			{
				return this.GetTable<RentalBook>();
			}
		}
		
		public System.Data.Linq.Table<Persons> Persons
		{
			get
			{
				return this.GetTable<Persons>();
			}
		}
		
		public System.Data.Linq.Table<Addresses> Addresses
		{
			get
			{
				return this.GetTable<Addresses>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Books")]
	public partial class Books : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _title;
		
		private string _isbn;
		
		private EntitySet<RentalBook> _RentalBook;
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OntitleChanging(string value);
    partial void OntitleChanged();
    partial void OnisbnChanging(string value);
    partial void OnisbnChanged();
    #endregion
		
		public Books()
		{
			this._RentalBook = new EntitySet<RentalBook>(new Action<RentalBook>(this.attach_RentalBook), new Action<RentalBook>(this.detach_RentalBook));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_title", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string title
		{
			get
			{
				return this._title;
			}
			set
			{
				if ((this._title != value))
				{
					this.OntitleChanging(value);
					this.SendPropertyChanging();
					this._title = value;
					this.SendPropertyChanged("title");
					this.OntitleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isbn", DbType="NVarChar(13) NOT NULL", CanBeNull=false)]
		public string isbn
		{
			get
			{
				return this._isbn;
			}
			set
			{
				if ((this._isbn != value))
				{
					this.OnisbnChanging(value);
					this.SendPropertyChanging();
					this._isbn = value;
					this.SendPropertyChanged("isbn");
					this.OnisbnChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Books_RentalBook", Storage="_RentalBook", ThisKey="id", OtherKey="bookId")]
		public EntitySet<RentalBook> RentalBook
		{
			get
			{
				return this._RentalBook;
			}
			set
			{
				this._RentalBook.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_RentalBook(RentalBook entity)
		{
			this.SendPropertyChanging();
			entity.Books = this;
		}
		
		private void detach_RentalBook(RentalBook entity)
		{
			this.SendPropertyChanging();
			entity.Books = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Rentals")]
	public partial class Rentals : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private int _personId;
		
		private System.DateTime _issueDate;
		
		private EntitySet<RentalBook> _RentalBook;
		
		private EntityRef<Persons> _Persons;
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnpersonIdChanging(int value);
    partial void OnpersonIdChanged();
    partial void OnissueDateChanging(System.DateTime value);
    partial void OnissueDateChanged();
    #endregion
		
		public Rentals()
		{
			this._RentalBook = new EntitySet<RentalBook>(new Action<RentalBook>(this.attach_RentalBook), new Action<RentalBook>(this.detach_RentalBook));
			this._Persons = default(EntityRef<Persons>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_personId", DbType="Int NOT NULL")]
		public int personId
		{
			get
			{
				return this._personId;
			}
			set
			{
				if ((this._personId != value))
				{
					if (this._Persons.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnpersonIdChanging(value);
					this.SendPropertyChanging();
					this._personId = value;
					this.SendPropertyChanged("personId");
					this.OnpersonIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_issueDate", DbType="DateTime NOT NULL")]
		public System.DateTime issueDate
		{
			get
			{
				return this._issueDate;
			}
			set
			{
				if ((this._issueDate != value))
				{
					this.OnissueDateChanging(value);
					this.SendPropertyChanging();
					this._issueDate = value;
					this.SendPropertyChanged("issueDate");
					this.OnissueDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Rentals_RentalBook", Storage="_RentalBook", ThisKey="id", OtherKey="rentalId")]
		public EntitySet<RentalBook> RentalBook
		{
			get
			{
				return this._RentalBook;
			}
			set
			{
				this._RentalBook.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Persons_Rentals", Storage="_Persons", ThisKey="personId", OtherKey="id", IsForeignKey=true)]
		public Persons Persons
		{
			get
			{
				return this._Persons.Entity;
			}
			set
			{
				Persons previousValue = this._Persons.Entity;
				if (((previousValue != value) 
							|| (this._Persons.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Persons.Entity = null;
						previousValue.Rentals.Remove(this);
					}
					this._Persons.Entity = value;
					if ((value != null))
					{
						value.Rentals.Add(this);
						this._personId = value.id;
					}
					else
					{
						this._personId = default(int);
					}
					this.SendPropertyChanged("Persons");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_RentalBook(RentalBook entity)
		{
			this.SendPropertyChanging();
			entity.Rentals = this;
		}
		
		private void detach_RentalBook(RentalBook entity)
		{
			this.SendPropertyChanging();
			entity.Rentals = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.RentalBook")]
	public partial class RentalBook : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private int _rentalId;
		
		private int _bookId;
		
		private System.Nullable<System.DateTime> _returnDate;
		
		private EntityRef<Books> _Books;
		
		private EntityRef<Rentals> _Rentals;
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnrentalIdChanging(int value);
    partial void OnrentalIdChanged();
    partial void OnbookIdChanging(int value);
    partial void OnbookIdChanged();
    partial void OnreturnDateChanging(System.Nullable<System.DateTime> value);
    partial void OnreturnDateChanged();
    #endregion
		
		public RentalBook()
		{
			this._Books = default(EntityRef<Books>);
			this._Rentals = default(EntityRef<Rentals>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_rentalId", DbType="Int NOT NULL")]
		public int rentalId
		{
			get
			{
				return this._rentalId;
			}
			set
			{
				if ((this._rentalId != value))
				{
					if (this._Rentals.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnrentalIdChanging(value);
					this.SendPropertyChanging();
					this._rentalId = value;
					this.SendPropertyChanged("rentalId");
					this.OnrentalIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_bookId", DbType="Int NOT NULL")]
		public int bookId
		{
			get
			{
				return this._bookId;
			}
			set
			{
				if ((this._bookId != value))
				{
					if (this._Books.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnbookIdChanging(value);
					this.SendPropertyChanging();
					this._bookId = value;
					this.SendPropertyChanged("bookId");
					this.OnbookIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_returnDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> returnDate
		{
			get
			{
				return this._returnDate;
			}
			set
			{
				if ((this._returnDate != value))
				{
					this.OnreturnDateChanging(value);
					this.SendPropertyChanging();
					this._returnDate = value;
					this.SendPropertyChanged("returnDate");
					this.OnreturnDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Books_RentalBook", Storage="_Books", ThisKey="bookId", OtherKey="id", IsForeignKey=true)]
		public Books Books
		{
			get
			{
				return this._Books.Entity;
			}
			set
			{
				Books previousValue = this._Books.Entity;
				if (((previousValue != value) 
							|| (this._Books.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Books.Entity = null;
						previousValue.RentalBook.Remove(this);
					}
					this._Books.Entity = value;
					if ((value != null))
					{
						value.RentalBook.Add(this);
						this._bookId = value.id;
					}
					else
					{
						this._bookId = default(int);
					}
					this.SendPropertyChanged("Books");
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Rentals_RentalBook", Storage="_Rentals", ThisKey="rentalId", OtherKey="id", IsForeignKey=true)]
		public Rentals Rentals
		{
			get
			{
				return this._Rentals.Entity;
			}
			set
			{
				Rentals previousValue = this._Rentals.Entity;
				if (((previousValue != value) 
							|| (this._Rentals.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Rentals.Entity = null;
						previousValue.RentalBook.Remove(this);
					}
					this._Rentals.Entity = value;
					if ((value != null))
					{
						value.RentalBook.Add(this);
						this._rentalId = value.id;
					}
					else
					{
						this._rentalId = default(int);
					}
					this.SendPropertyChanged("Rentals");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Persons")]
	public partial class Persons : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _firstName;
		
		private string _lastName;
		
		private int _addressId;
		
		private EntitySet<Rentals> _Rentals;
		
		private EntityRef<Addresses> _Addresses;
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnfirstNameChanging(string value);
    partial void OnfirstNameChanged();
    partial void OnlastNameChanging(string value);
    partial void OnlastNameChanged();
    partial void OnaddressIdChanging(int value);
    partial void OnaddressIdChanged();
    #endregion
		
		public Persons()
		{
			this._Rentals = new EntitySet<Rentals>(new Action<Rentals>(this.attach_Rentals), new Action<Rentals>(this.detach_Rentals));
			this._Addresses = default(EntityRef<Addresses>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_firstName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string firstName
		{
			get
			{
				return this._firstName;
			}
			set
			{
				if ((this._firstName != value))
				{
					this.OnfirstNameChanging(value);
					this.SendPropertyChanging();
					this._firstName = value;
					this.SendPropertyChanged("firstName");
					this.OnfirstNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_lastName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string lastName
		{
			get
			{
				return this._lastName;
			}
			set
			{
				if ((this._lastName != value))
				{
					this.OnlastNameChanging(value);
					this.SendPropertyChanging();
					this._lastName = value;
					this.SendPropertyChanged("lastName");
					this.OnlastNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_addressId", DbType="Int NOT NULL")]
		public int addressId
		{
			get
			{
				return this._addressId;
			}
			set
			{
				if ((this._addressId != value))
				{
					if (this._Addresses.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnaddressIdChanging(value);
					this.SendPropertyChanging();
					this._addressId = value;
					this.SendPropertyChanged("addressId");
					this.OnaddressIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Persons_Rentals", Storage="_Rentals", ThisKey="id", OtherKey="personId")]
		public EntitySet<Rentals> Rentals
		{
			get
			{
				return this._Rentals;
			}
			set
			{
				this._Rentals.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Addresses_Persons", Storage="_Addresses", ThisKey="addressId", OtherKey="id", IsForeignKey=true)]
		public Addresses Addresses
		{
			get
			{
				return this._Addresses.Entity;
			}
			set
			{
				Addresses previousValue = this._Addresses.Entity;
				if (((previousValue != value) 
							|| (this._Addresses.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Addresses.Entity = null;
						previousValue.Persons.Remove(this);
					}
					this._Addresses.Entity = value;
					if ((value != null))
					{
						value.Persons.Add(this);
						this._addressId = value.id;
					}
					else
					{
						this._addressId = default(int);
					}
					this.SendPropertyChanged("Addresses");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Rentals(Rentals entity)
		{
			this.SendPropertyChanging();
			entity.Persons = this;
		}
		
		private void detach_Rentals(Rentals entity)
		{
			this.SendPropertyChanging();
			entity.Persons = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Addresses")]
	public partial class Addresses : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _id;
		
		private string _street;
		
		private string _zip;
		
		private string _city;
		
		private string _country;
		
		private EntitySet<Persons> _Persons;
		
    #region Definitionen der Erweiterungsmethoden
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnidChanging(int value);
    partial void OnidChanged();
    partial void OnstreetChanging(string value);
    partial void OnstreetChanged();
    partial void OnzipChanging(string value);
    partial void OnzipChanged();
    partial void OncityChanging(string value);
    partial void OncityChanged();
    partial void OncountryChanging(string value);
    partial void OncountryChanged();
    #endregion
		
		public Addresses()
		{
			this._Persons = new EntitySet<Persons>(new Action<Persons>(this.attach_Persons), new Action<Persons>(this.detach_Persons));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this.OnidChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("id");
					this.OnidChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_street", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string street
		{
			get
			{
				return this._street;
			}
			set
			{
				if ((this._street != value))
				{
					this.OnstreetChanging(value);
					this.SendPropertyChanging();
					this._street = value;
					this.SendPropertyChanged("street");
					this.OnstreetChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_zip", DbType="NVarChar(32) NOT NULL", CanBeNull=false)]
		public string zip
		{
			get
			{
				return this._zip;
			}
			set
			{
				if ((this._zip != value))
				{
					this.OnzipChanging(value);
					this.SendPropertyChanging();
					this._zip = value;
					this.SendPropertyChanged("zip");
					this.OnzipChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_city", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string city
		{
			get
			{
				return this._city;
			}
			set
			{
				if ((this._city != value))
				{
					this.OncityChanging(value);
					this.SendPropertyChanging();
					this._city = value;
					this.SendPropertyChanged("city");
					this.OncityChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_country", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string country
		{
			get
			{
				return this._country;
			}
			set
			{
				if ((this._country != value))
				{
					this.OncountryChanging(value);
					this.SendPropertyChanging();
					this._country = value;
					this.SendPropertyChanged("country");
					this.OncountryChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Addresses_Persons", Storage="_Persons", ThisKey="id", OtherKey="addressId")]
		public EntitySet<Persons> Persons
		{
			get
			{
				return this._Persons;
			}
			set
			{
				this._Persons.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Persons(Persons entity)
		{
			this.SendPropertyChanging();
			entity.Addresses = this;
		}
		
		private void detach_Persons(Persons entity)
		{
			this.SendPropertyChanging();
			entity.Addresses = null;
		}
	}
}
#pragma warning restore 1591
