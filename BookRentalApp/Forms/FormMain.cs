﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace BookRentalApp
{
    public partial class FormMain : Form
    {
        // Use the following connection string.
        BookRentalAppDb db = new BookRentalAppDb(@"Data Source=DESKTOP-NML6L7C\SQLEXPRESS;Initial Catalog=BookRentalAppDb;Integrated Security=True");

        public FormMain()
        {
            InitializeComponent();
        }

        private void tabPageBooks_Enter(object sender, EventArgs e)
        {
            var allBooks = (from book in db.Books
                            select book);
            bindingSourceBooks.DataSource = allBooks;
            dataGridViewBooks.DataSource = bindingSourceBooks;
            bindingNavigatorBooks.BindingSource = bindingSourceBooks;
        }

        private void speichernToolStripButton_Click(object sender, EventArgs e)
        {
            if (bindingNavigatorBooks.Validate()) MessageBox.Show("Validation Success!", "Validation...", MessageBoxButtons.OK);
            try { db.SubmitChanges(); }
            catch (System.Data.SqlClient.SqlException ex)
            {
                MessageBox.Show(ex.GetType().FullName + "\n" + ex.Message, "Error...", MessageBoxButtons.OK);
            }
        }
    }
}
