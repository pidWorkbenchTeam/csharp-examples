﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Linq2SqlBookRentalFull
{
    public partial class FrmMain : Form
    {
        DataClassesDataContext dataContext = new DataClassesDataContext();
        /* 
        enum State { Rentals, Books, Persons, Addresses, Unknown };
        State state = State.Unknown;
        string foreignId;
        string primaryId;
        switch(state)
            {
                case State.Rentals:
                    break;
                case State.Books:
                    break;
                case State.Persons:
                    break;
                case State.Addresses:
                    break;
            }
        */

        public class BookWithDate
        {
            public System.Int32 BookId
            { get; set; }
            public System.String Isbn
            { get; set; }
            public System.String Title
            { get; set; }
            public System.Nullable<System.DateTime> ReturnDate
            { get; set; }
        }

        public FrmMain()
        {
            InitializeComponent();
        }

        private void tpAddresses_Enter(object sender, EventArgs e)
        {
            dgvAddresses.DataSource = dataContext.GetTable<Addresses>();
        }
        private void tpPersons_Enter(object sender, EventArgs e)
        {
            dgvPersons.DataSource = dataContext.GetTable<Persons>();
        }
        private void tpBooks_Enter(object sender, EventArgs e)
        {
            dgvBooks.DataSource = dataContext.GetTable<Books>();
        }
        private void tpRentals_Enter(object sender, EventArgs e)
        {
            /*var query = (from rentals in dataContext.GetTable<Rental>()
                         from persons in dataContext.GetTable<Person>()
                         from books in dataContext.GetTable<Book>()
                         from rbs in dataContext.GetTable<RentalBooksRelationship>()
                         where ((rentals.personId == persons.id) &&
                                (rbs.rentalId == rentals.id) &&
                                (rbs.bookId == books.id))
                         orderby rentals.issueDate
                         select new BookRentals
                         {
                             RentalId = rentals.id,
                             IssueDate = rentals.issueDate,
                             Firstname = persons.firstname,
                             Lastname = persons.lastname,
                             BookTitle = books.title,
                             BookISBN = books.isbn,
                             ReturnDate = rbs.returnDate
                         }).ToList<BookRentals>();*/
            dgvRentedBooks.DataSource = dataContext.GetTable<RentalView>();
            dgvRental.DataSource = dataContext.GetTable<Rentals>();
            // ToDo delete
            var query = (from books in dataContext.GetTable<Books>()
                         from rbs in dataContext.GetTable<RentalBooksRelationship>()
                         where ((rbs.rentalId == (int)dgvRental.CurrentRow.Cells[0].Value) &&
                                (books.id == rbs.bookId))
                         select books);
            dgvRentalBooks.DataSource = query;
        }
        private void tpRentedBooks_Enter(object sender, EventArgs e)
        {
            var query = (from rentals in dataContext.GetTable<Rentals>()
                         from persons in dataContext.GetTable<Persons>()
                         from books in dataContext.GetTable<Books>()
                         from rbs in dataContext.GetTable<RentalBooksRelationship>()
                         where ((rbs.returnDate == null) &&
                                (rentals.personId == persons.id) &&
                                (rbs.rentalId == rentals.id) &&
                                (rbs.bookId == books.id))
                         orderby rentals.issueDate
                         select new RentalView()
                         {
                             rentalId = rentals.id,
                             issueDate = rentals.issueDate,
                             firstname = persons.firstname,
                             lastname = persons.lastname,
                             title = books.title,
                             isbn = books.isbn,
                             returnDate = rbs.returnDate
                         }).ToList<RentalView>();
            dgvRentalBooks.DataSource = query;
        }

        private void tpAddresses_Leave(object sender, EventArgs e)
        {
            if (btnSelectAddresses.Visible)
            {
                hideButton(btnSelectAddresses, btnSaveAddresses);
            }
        }
        private void tpBooks_Leave(object sender, EventArgs e)
        {
            if (btnSelectBooks.Visible)
            {
                hideButton(btnSelectBooks, btnSaveBooks);
            }
        }
        private void tpPersons_Leave(object sender, EventArgs e)
        {
            if (btnSelectPersons.Visible)
            {
                hideButton(btnSelectPersons, btnSavePersons);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                dataContext.SubmitChanges();
                MessageBox.Show("Data was saved successfully!", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
        }

        private void dgvAddresses_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {            
            e.Row.Cells[0].Value = dataContext.Addresses.Max(a => a.id) + 1;
        }
        private void dgvPersons_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[0].Value = dataContext.Persons.Max(p => p.id) + 1;
        }
        private void dgvBooks_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[0].Value = dataContext.Books.Max(b => b.id) + 1;
        }
        private void dgvRentals_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells["tbcRentalsRentalId"].Value = dataContext.Rentals.Max(r => r.id) + 1;
        }

        private void btnSearchAddresses_Click(object sender, EventArgs e)
        {
            var query = from address in dataContext.GetTable<Addresses>() select address;
            if (!string.IsNullOrWhiteSpace(tbAddressesStreet.Text))
            {
                query = query.Where(address => address.street.Contains(tbAddressesStreet.Text));
            }
            if (!string.IsNullOrWhiteSpace(tbAddressesNumber.Text))
            {
                query = query.Where(address => address.number.Contains(tbAddressesNumber.Text));
            }
            if (!string.IsNullOrWhiteSpace(tbAddressesZip.Text))
            {
                query = query.Where(address => address.zip.Contains(tbAddressesZip.Text));
            }
            if (!string.IsNullOrWhiteSpace(tbAddressesCountry.Text))
            {
                query = query.Where(address => address.country.Contains(tbAddressesCountry.Text));
            }
            dgvAddresses.DataSource = query;
        }
        private void btnSearchPersons_Click(object sender, EventArgs e)
        {
            var query = from person in dataContext.GetTable<Persons>() select person;
            if (!string.IsNullOrWhiteSpace(tbPersonsFirstname.Text))
            {
                query = query.Where(persons => persons.firstname.Contains(tbPersonsFirstname.Text));
            }
            if (!string.IsNullOrWhiteSpace(tbPersonsLastname.Text))
            {
                query = query.Where(persons => persons.lastname.Contains(tbPersonsLastname.Text));
            }
            dgvPersons.DataSource = query;
        }
        private void btnSearchBooks_Click(object sender, EventArgs e)
        {
            var query = from book in dataContext.GetTable<Books>() select book;
            if (!string.IsNullOrWhiteSpace(tbBooksTitle.Text))
            {
                query = query.Where(books => books.title.Contains(tbBooksTitle.Text));
            }
            if (!string.IsNullOrWhiteSpace(tbBooksIsbn.Text))
            {
                query = query.Where(books => books.title.Contains(tbBooksIsbn.Text));
            }
            dgvBooks.DataSource = query;
        }

        private void dgvPersons_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvPersons.Columns["tbcPersonsAddresses"].Index)
            {
                addButton(btnSelectAddresses, btnSaveAddresses);
                tcMain.SelectTab(tpAddresses);
                //this.row = 
            }
        }
        private void dgvRentals_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvRentedBooks.Columns["tbcRentalsFirstname"].Index)
            {
                addButton(btnSelectPersons, btnSavePersons);
                tcMain.SelectTab(tpPersons);
            }
            if (e.ColumnIndex == dgvRentedBooks.Columns["tbcRentalsTitle"].Index)
            {
                addButton(btnSelectBooks, btnSaveBooks);
                tcMain.SelectTab(tpBooks);
            }
        }

        private void btnSelectAddresses_Click(object sender, EventArgs e)
        {
            //this.foreignId = dgvAddresses.CurrentRow.Cells[0].Value.ToString();
            dgvPersons.CurrentRow.Cells["tbcPersonsAddressId"].Value = dgvAddresses.CurrentRow.Cells[0].Value;
            dgvPersons.CurrentRow.Cells["tbcPersonsAddresses"].Value = dataContext.Addresses.Where(a => a.id == (int)dgvAddresses.CurrentRow.Cells[0].Value).Single();
            //dgvAddresses.SelectedRow
            //dgvPersons.Row
            tcMain.SelectTab(tpPersons);
        }
        private void btnSelectBooks_Click(object sender, EventArgs e)
        {
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsBookId"].Value = dgvBooks.CurrentRow.Cells["tbcBooksId"].Value;
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsTitle"].Value = dataContext.Books.Where(b => b.id == (int)dgvBooks.CurrentRow.Cells["tbcBooksId"].Value).Single().title.ToString();
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsIsbn"].Value = dataContext.Books.Where(b => b.id == (int)dgvBooks.CurrentRow.Cells["tbcBooksId"].Value).Single().isbn.ToString();
            tcMain.SelectTab(tpRentals);
        }
        private void btnSelectPersons_Click(object sender, EventArgs e)
        {
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsPersonId"].Value = dgvPersons.CurrentRow.Cells["tbcPersonsId"].Value; 
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsFirstname"].Value = dataContext.Persons.Where(p => p.id == (int)dgvPersons.CurrentRow.Cells["tbcPersonsId"].Value).Single().firstname;
            dgvRentedBooks.CurrentRow.Cells["tbcRentalsLastname"].Value = dataContext.Persons.Where(p => p.id == (int)dgvPersons.CurrentRow.Cells["tbcPersonsId"].Value).Single().lastname;
            tcMain.SelectTab(tpRentals);
        }

        public void addButton(Button newbtn, Button btn)
        {
            btn.Location = new System.Drawing.Point(btn.Location.X, btn.Location.Y + newbtn.Height);
            btn.Size = new System.Drawing.Size(btn.Width, btn.Height - newbtn.Height);
            //btnSaveAddresses.Refresh();
            newbtn.Visible = true;
        }
        public void hideButton(Button newbtn, Button btn)
        {
            btn.Location = new System.Drawing.Point(btn.Location.X, btn.Location.Y - newbtn.Height);
            btn.Size = new System.Drawing.Size(btn.Width, btn.Height + newbtn.Height);
            //btnSaveAddresses.Refresh();
            newbtn.Visible = false;
        }

        private void btnSelectPerson_Click(object sender, EventArgs e)
        {
            addButton(btnSelectPersons, btnSavePersons);
            tcMain.SelectTab(tpPersons);
        }

        private void dgvRental_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            var bookquery = (from books in dataContext.GetTable<Books>()
                             from rbs in dataContext.GetTable<RentalBooksRelationship>()
                             where ((rbs.rentalId == (int)dgvRental.CurrentRow.Cells[0].Value) &&
                                    (books.id == rbs.bookId))
                             select new BookWithDate
                             {
                                 BookId = books.id,
                                 Isbn = books.isbn,
                                 Title = books.title,
                                 ReturnDate = rbs.returnDate
                             }).ToList<BookWithDate>();
            dgvRentalBooks.DataSource = bookquery;

            var personquery = (from persons in dataContext.GetTable<Persons>()
                               from rentals in dataContext.GetTable<Rentals>()
                               where ((rentals.id == (int)dgvRental.CurrentRow.Cells[0].Value) &&
                                      (persons.id == rentals.personId))
                               select persons).FirstOrDefault();
            tbAddress.Text = personquery.Addresses.ToString();
            /*
            if (personquery != null)
            {
                tbAddress.Text = personquery.Addresses.ToString();
            }
            else tbAddress.Text = "";
            */
            /*
            tbFirstname.DataBindings.Add("Text", personquery.firstname, "firstname");
            tbLastname.DataBindings.Add("Text", personquery.lastname, "lastname");
            tbAddress.DataBindings.Add("Text", personquery.Addresses, "Addresses");
            */
        }

        private void btnShowRentedBooks_Click(object sender, EventArgs e)
        {
            var query = (from rentals in dataContext.GetTable<Rentals>()
                         from persons in dataContext.GetTable<Persons>()
                         from books in dataContext.GetTable<Books>()
                         from rbs in dataContext.GetTable<RentalBooksRelationship>()
                         where ((rbs.returnDate == null) &&
                                (rentals.personId == persons.id) &&
                                (rbs.rentalId == rentals.id) &&
                                (rbs.bookId == books.id))
                         orderby rentals.issueDate
                         select new RentalView()
                         {
                             rentalId = rentals.id,
                             issueDate = rentals.issueDate,
                             firstname = persons.firstname,
                             lastname = persons.lastname,
                             title = books.title,
                             isbn = books.isbn,
                             returnDate = rbs.returnDate
                         }).ToList<RentalView>();
            dgvRentalBooks.DataSource = query;
        }

        private void btnNewRental_Click(object sender, EventArgs e)
        {
            NewRental newRental = new NewRental();
            newRental.ShowDialog();
        }

        private void btnDeleteRental_Click(object sender, EventArgs e)
        {
            /*
            var queryrbs = (from rbs in dataContext.RentalBooksRelationship
                            where(rbs.rentalId == (int)dgvRental.CurrentRow.Cells[0].Value)
                            select rbs);
            dataContext.RentalBooksRelationship.DeleteAllOnSubmit(queryrbs);
            */
            int dgvRowIndex = dgvRental.CurrentRow.Index-1;
            Rentals rental = (Rentals)dgvRental.CurrentRow.DataBoundItem;
            dataContext.RentalBooksRelationship.DeleteAllOnSubmit(rental.RentalBooksRelationship);
            dataContext.Rentals.DeleteOnSubmit(rental);
            dataContext.SubmitChanges();
            dataContext = new DataClassesDataContext();
            dgvRental.DataSource = dataContext.GetTable<Rentals>();
            dgvRental.CurrentCell = dgvRental.Rows[dgvRowIndex].Cells[0];
            //dgvRental.Rows[dgvRowIndex].Selected = true;
            //DataRow row = (dataGridView1.SelectedRows[0].DataBoundItem as DataRowView).Row;
            //Person selected = (Person)row;
        }
    }
}
