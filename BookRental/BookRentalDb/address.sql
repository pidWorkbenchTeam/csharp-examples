﻿CREATE TABLE [dbo].[address]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(0, 1), 
    [street] NVARCHAR(64) NOT NULL, 
    [zip] NVARCHAR(32) NOT NULL, 
    [city] NVARCHAR(64) NOT NULL, 
    [country] NVARCHAR(64) NOT NULL
)
