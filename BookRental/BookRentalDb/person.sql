﻿CREATE TABLE [dbo].[person]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(0, 1), 
    [lastName] NVARCHAR(64) NOT NULL, 
    [firstName] NVARCHAR(64) NOT NULL, 
    [addressId] INT NOT NULL, 
    CONSTRAINT [FK_person_address] FOREIGN KEY (addressId) REFERENCES address(id)
)
