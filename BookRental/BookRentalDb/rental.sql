﻿CREATE TABLE [dbo].[rental]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1), 
    [personId] INT NOT NULL, 
    [bookId] INT NOT NULL, 
    [issueDate] DATETIME NOT NULL, 
    [returnDate] DATETIME NULL, 
    CONSTRAINT [FK_rental_person] FOREIGN KEY (personId) REFERENCES person(id), 
    CONSTRAINT [FK_rental_book] FOREIGN KEY (bookId) REFERENCES book(id) 
)
