﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookRental
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DialogResult loginResult;
            using (LoginForm loginForm = new LoginForm())
            {
                loginResult = loginForm.ShowDialog();
            }
            if (loginResult == DialogResult.OK)
            {
                Application.Run(new RentalForm());
            }
        }
    }
}
