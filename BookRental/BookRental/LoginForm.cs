﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
//using System.IO;

namespace BookRental
{
    /// <summary>
    /// The login form.
    /// </summary>
    public partial class LoginForm : Form
    {
        /// <summary>
        /// Store for the computerName_ property.
        /// </summary>
        private string computerName_;

        /// <summary>
        /// The class constructor.
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();
            this.pbLoginStatus.Image = new Bitmap(@"..\..\key.png");
        }

        /// <summary>
        /// The btLogin_Click Method. When the login button is clicked the btLogin_Click event handler connects to the database compares username and hashed password and opens RentalForm if it matches.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void btLogin_Click(object sender, EventArgs e)
        {
            computerName_ = System.Windows.Forms.SystemInformation.ComputerName;
            //MessageBox.Show(computerName_);
            SqlConnection scn = new SqlConnection();
            scn.ConnectionString = "Data Source=" + computerName_ + "\\SQLEXPRESS;Initial Catalog=login_credentials;database=BookRentalDb;integrated security=SSPI";
            //string sqlStr = "INSERT INTO dbo.login_credentials (username, password) VALUES (\'" + tbUsername.Text + "\', HASHBYTES(\'SHA2_512\', \'" + tbPassword.Text + "\'))";

            string sqlStr = "SELECT COUNT (*) AS cnt FROM dbo.login_credentials WHERE username='" + tbUsername.Text + "\' AND password=HASHBYTES('SHA2_512', \'" + tbPassword.Text + "\')";

            //MessageBox.Show(sqlStr);
            SqlCommand scmd = new SqlCommand(sqlStr, scn);

            //System.IO.File.WriteAllText(@".\WriteText.txt", tbPassword.Text);

            scn.Open();

            if (scmd.ExecuteScalar().ToString() == "1")
            {
                this.pbLoginStatus.Image = new Bitmap(@"..\..\open.jpg");
                tbUsername.Clear();
                tbPassword.Clear();
                this.DialogResult = DialogResult.OK;
                /*
                RentalForm rentalForm = new RentalForm();
                this.Hide();
                rentalForm.ShowDialog();
                */
                this.Close();
            }
            else
            {
                this.pbLoginStatus.Image = new Bitmap(@"..\..\closed.jpg");
                MessageBox.Show("Password or username not found.");
                tbUsername.Clear();
                tbPassword.Clear();
            }

            scn.Close();
        }

        /// <summary>
        /// The btCancel_Click Method. When the cancel button is clicked the btCancel_Click event handler closes the LoginForm.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
