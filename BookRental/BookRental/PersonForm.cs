﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookRental
{
    /// <summary>
    /// The PersonForm class loads the person data from Database and shows it in a Datagrid.
    /// </summary>
    public partial class PersonForm : Form
    {
        public string personId_ { get; set; }
        public string firstName_ { get; set; }
        public string lastName_ { get; set; }

        /// <summary>
        /// The class constructor. Initializes the components.
        /// </summary>
        public PersonForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The personBindingNavigatorSaveItem_Click Method saves new lines and updates changes from dataGrid to the database on save button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void personBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.personBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bookRentalDbDataSet);
        }

        /// <summary>
        /// The PersonForm_Load Method fills the addressTableAdapter.
        /// </summary>
        /// <param name="sender"> Contains a reference to the event.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void PersonForm_Load(object sender, EventArgs e)
        {
            // TODO: Diese Codezeile lädt Daten in die Tabelle "bookRentalDbDataSet.person". Sie können sie bei Bedarf verschieben oder entfernen.
            this.personTableAdapter.Fill(this.bookRentalDbDataSet.person);

        }

        /// <summary>
        /// The personDataGridView_CellContentDoubleClick Method saves the data of the current row to the respective members on cell content doubleclick.
        /// </summary>
        /// <param name="sender"> Contains a reference to the dataGridView cell that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.Windows.Forms.DataGridViewCellEventArgs></seealso>
        private void personDataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.personId_ = personDataGridView.CurrentRow.Cells[0].Value.ToString();
            this.firstName_ = personDataGridView.CurrentRow.Cells[1].Value.ToString();
            this.lastName_ = personDataGridView.CurrentRow.Cells[2].Value.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// The personDataGridView_CellDoubleClick Method opens a new AddressForm when clicked on the address column.
        /// </summary>
        /// <param name="sender"> Contains a reference to the dataGridView cell that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.Windows.Forms.DataGridViewCellEventArgs></seealso>
        private void personDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                AddressForm addressForm = new AddressForm();
                if (addressForm.ShowDialog() == DialogResult.OK)
                {
                    personDataGridView.CurrentRow.Cells[3].Value = addressForm.addressId_;
                    personDataGridView.RefreshEdit();
                }
            }
        }
    }
}
