﻿namespace BookRental
{
    partial class RentalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rentalDataGridView = new System.Windows.Forms.DataGridView();
            this.addressBtn = new System.Windows.Forms.Button();
            this.bookBtn = new System.Windows.Forms.Button();
            this.personBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rentalDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rentalDataGridView
            // 
            this.rentalDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rentalDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rentalDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rentalDataGridView.Location = new System.Drawing.Point(-1, 1);
            this.rentalDataGridView.Name = "rentalDataGridView";
            this.rentalDataGridView.RowTemplate.Height = 24;
            this.rentalDataGridView.Size = new System.Drawing.Size(693, 517);
            this.rentalDataGridView.TabIndex = 0;
            this.rentalDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.rentalDataGridView_CellDoubleClick);
            // 
            // addressBtn
            // 
            this.addressBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addressBtn.Location = new System.Drawing.Point(711, 11);
            this.addressBtn.Name = "addressBtn";
            this.addressBtn.Size = new System.Drawing.Size(129, 33);
            this.addressBtn.TabIndex = 1;
            this.addressBtn.Text = "New Address";
            this.addressBtn.UseVisualStyleBackColor = true;
            this.addressBtn.Click += new System.EventHandler(this.addressBtn_Click);
            // 
            // bookBtn
            // 
            this.bookBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bookBtn.Location = new System.Drawing.Point(711, 109);
            this.bookBtn.Name = "bookBtn";
            this.bookBtn.Size = new System.Drawing.Size(129, 33);
            this.bookBtn.TabIndex = 2;
            this.bookBtn.Text = "New Book";
            this.bookBtn.UseVisualStyleBackColor = true;
            this.bookBtn.Click += new System.EventHandler(this.bookBtn_Click);
            // 
            // personBtn
            // 
            this.personBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.personBtn.Location = new System.Drawing.Point(711, 60);
            this.personBtn.Name = "personBtn";
            this.personBtn.Size = new System.Drawing.Size(129, 33);
            this.personBtn.TabIndex = 3;
            this.personBtn.Text = "New Person";
            this.personBtn.UseVisualStyleBackColor = true;
            this.personBtn.Click += new System.EventHandler(this.personBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveBtn.Location = new System.Drawing.Point(711, 472);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(129, 33);
            this.saveBtn.TabIndex = 4;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // RentalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 517);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.personBtn);
            this.Controls.Add(this.bookBtn);
            this.Controls.Add(this.addressBtn);
            this.Controls.Add(this.rentalDataGridView);
            this.Name = "RentalForm";
            this.Text = "Rentals";
            this.Load += new System.EventHandler(this.RentalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rentalDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView rentalDataGridView;
        private System.Windows.Forms.Button addressBtn;
        private System.Windows.Forms.Button bookBtn;
        private System.Windows.Forms.Button personBtn;
        private System.Windows.Forms.Button saveBtn;
    }
}