﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookRental
{
    /// <summary>
    /// The AddressForm class loads the address data from Database and shows it in a Datagrid.
    /// </summary>
    public partial class AddressForm : Form
    {
        public string addressId_ { get; set; }

        /// <summary>
        /// The class constructor. Initializes the components.
        /// </summary>
        public AddressForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The addressBindingNavigatorSaveItem_Click Method saves new lines and updates changes from dataGrid to the database on save button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void addressBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.addressBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bookRentalDbDataSet);

        }

        /// <summary>
        /// The AddressForm_Load Method fills the addressTableAdapter.
        /// </summary>
        /// <param name="sender"> Contains a reference to the event.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void AddressForm_Load(object sender, EventArgs e)
        {
            // TODO: Diese Codezeile lädt Daten in die Tabelle "bookRentalDbDataSet.address". Sie können sie bei Bedarf verschieben oder entfernen.
            this.addressTableAdapter.Fill(this.bookRentalDbDataSet.address);

        }

        /// <summary>
        /// The addressDataGridView_CellContentDoubleClick Method saves the id value of the current row to addressId_ on cell content doubleclick.
        /// </summary>
        /// <param name="sender"> Contains a reference to the dataGridView cell that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.Windows.Forms.DataGridViewCellEventArgs></seealso>
        private void addressDataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.addressId_ = addressDataGridView.CurrentRow.Cells[0].Value.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
