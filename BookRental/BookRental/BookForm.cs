﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookRental
{
    /// <summary>
    /// The BookForm class loads the book data from Database and shows it in a Datagrid.
    /// </summary>
    public partial class BookForm : Form
    {
        public string bookId_ { get; set; }
        public string bookTitle_ { get; set; }

        /// <summary>
        /// The class constructor. Initializes the components.
        /// </summary>
        public BookForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The bookBindingNavigatorSaveItem_Click Method saves new lines and updates changes from dataGrid to the database on save button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void bookBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bookBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bookRentalDbDataSet);

        }

        /// <summary>
        /// The BookForm_Load Method fills the bookTableAdapter.
        /// </summary>
        /// <param name="sender"> Contains a reference to the event.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void BookForm_Load(object sender, EventArgs e)
        {
            // TODO: Diese Codezeile lädt Daten in die Tabelle "bookRentalDbDataSet.book". Sie können sie bei Bedarf verschieben oder entfernen.
            this.bookTableAdapter.Fill(this.bookRentalDbDataSet.book);

        }

        /// <summary>
        /// The bookDataGridView_CellContentDoubleClick Method saves the id and bookTitle value of the current row to the respective members on cell content doubleclick.
        /// </summary>
        /// <param name="sender"> Contains a reference to the dataGridView cell that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.Windows.Forms.DataGridViewCellEventArgs></seealso>
        private void bookDataGridView_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.bookId_ = bookDataGridView.CurrentRow.Cells[0].Value.ToString();
            this.bookTitle_ = bookDataGridView.CurrentRow.Cells[1].Value.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
