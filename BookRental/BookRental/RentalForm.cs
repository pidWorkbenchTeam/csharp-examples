﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace BookRental
{
    /// <summary>
    /// The RentalForm loads the rental data from Database and loads it in a Datagrid.
    /// </summary>
    public partial class RentalForm : Form
    {
        /// <summary>
        /// Store for the sqlCon property.
        /// </summary>
        SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-NML6L7C\SQLEXPRESS;Initial Catalog=BookRentalDb;Integrated Security=True");
        /// <summary>
        /// Store for the sqlCmd property.
        /// </summary>
        SqlCommand sqlCmd;
        /// <summary>
        /// Store for the sqlParam property.
        /// </summary>
        SqlParameter sqlParam;
        /// <summary>
        /// Store for the dataTable property.
        /// </summary>
        DataTable dataTable;
        /// <summary>
        /// Store for the dataAdapter property.
        /// </summary>
        SqlDataAdapter dataAdapter;

        /// <summary>
        /// The class constructor. Adds update, insert and delete command to the dataAdapter.
        /// </summary>
        public RentalForm()
        {
            InitializeComponent();
            dataTable = new DataTable();

            // SELECT
            dataAdapter = new SqlDataAdapter("SELECT person.firstName, person.lastName, book.title AS bookTitle, rental.issueDate, rental.returnDate, rental.id, rental.personId, rental.bookId " +
                                             "FROM rental INNER JOIN " +
                                             "  person ON rental.personId = person.Id INNER JOIN " +
                                             "  book ON rental.bookId = book.Id", sqlCon);
            // INSERT
            sqlCmd = new SqlCommand("INSERT INTO rental(personId, bookId, issueDate, returnDate) " +
                                "VALUES (@personId, @bookId, GETDATE(), @returnDate)", sqlCon);
            sqlCmd.Parameters.Add("@personId", SqlDbType.Int, 8, "personId");
            sqlCmd.Parameters.Add("@bookId", SqlDbType.Int, 8, "bookId");
            sqlCmd.Parameters.Add("@returnDate", SqlDbType.DateTime, 4, "returnDate");
            dataAdapter.InsertCommand = sqlCmd;

            // UPDATE
            sqlCmd = new SqlCommand("UPDATE rental " +
                                    "SET personId = @personId, bookId = @bookId, issueDate = @issueDate, returnDate = @returnDate " +
                                    "WHERE id = @id", sqlCon);
            sqlCmd.Parameters.Add("@personId", SqlDbType.Int, 8, "personId");
            sqlCmd.Parameters.Add("@bookId", SqlDbType.Int, 8, "bookId");
            sqlCmd.Parameters.Add("@returnDate", SqlDbType.DateTime, 4, "returnDate");
            sqlCmd.Parameters.Add("@issueDate", SqlDbType.DateTime, 4, "issueDate");
            sqlParam = sqlCmd.Parameters.Add("@id", SqlDbType.Int, 8, "id");
            sqlParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.UpdateCommand = sqlCmd;

            // DELETE
            sqlCmd = new SqlCommand("DELETE FROM rental " +
                                    "WHERE id = @id", sqlCon);
            sqlParam = sqlCmd.Parameters.Add("@id", SqlDbType.Int, 8, "id");
            sqlParam.SourceVersion = DataRowVersion.Original;
            dataAdapter.DeleteCommand = sqlCmd;
        }

        /// <summary>
        /// The displayView Method fills the dataGridView and hides the id columns.
        /// </summary>
        private void displayView()
        {
            dataTable.Clear();
            dataAdapter.Fill(dataTable);
            rentalDataGridView.DataSource = dataTable;
            for (int i = rentalDataGridView.Columns["id"].Index; i <= rentalDataGridView.Columns["bookId"].Index; ++i)
            {
                rentalDataGridView.Columns[i].Visible = false;
            }
        }

        /// <summary>
        /// The addressBtn_Click Method opens a new AddressForm on address button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void addressBtn_Click(object sender, EventArgs e)
        {
            AddressForm addressForm = new AddressForm();
            addressForm.ShowDialog();
        }

        /// <summary>
        /// The personBtn_Click Method opens a new PersonForm on person button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void personBtn_Click(object sender, EventArgs e)
        {
            PersonForm personForm = new PersonForm();
            personForm.ShowDialog();
        }

        /// <summary>
        /// The bookBtn_Click Method opens a new BookForm on book button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void bookBtn_Click(object sender, EventArgs e)
        {
            BookForm bookForm = new BookForm();
            bookForm.ShowDialog();
        }

        /// <summary>
        /// The rentalDataGridView_CellDoubleClick Method opens a new form respective to the column that was clicked.
        /// </summary>
        /// <param name="sender">Contains a reference to the dataGridView cell that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e">Contains the event data.</param>
        /// <seealso cref="System.Windows.Forms.DataGridViewCellEventArgs"></seealso>
        private void rentalDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                case 1:
                    PersonForm personForm = new PersonForm();
                    if (personForm.ShowDialog() == DialogResult.OK)
                    {
                        rentalDataGridView.CurrentRow.Cells["firstName"].Value = personForm.firstName_;
                        rentalDataGridView.CurrentRow.Cells["lastName"].Value = personForm.lastName_;
                        rentalDataGridView.CurrentRow.Cells["personId"].Value = personForm.personId_;
                    }
                    break;
                case 2:
                    BookForm bookForm = new BookForm();
                    if (bookForm.ShowDialog() == DialogResult.OK)
                    {
                        rentalDataGridView.CurrentRow.Cells["bookTitle"].Value = bookForm.bookTitle_;
                        rentalDataGridView.CurrentRow.Cells["bookId"].Value = bookForm.bookId_;
                    }
                    break;
                case 4:
                    if (rentalDataGridView.CurrentRow.Cells["returnDate"].Value == null ||
                        rentalDataGridView.CurrentRow.Cells["returnDate"].Value == DBNull.Value ||
                        String.IsNullOrWhiteSpace(rentalDataGridView.CurrentRow.Cells["returnDate"].Value.ToString()))
                    {
                        rentalDataGridView.CurrentRow.Cells["returnDate"].Value = DateTime.Now.ToString();
                        rentalDataGridView.RefreshEdit();
                    }
                    break;
                default:
                    MessageBox.Show("Error: Column \"" + @rentalDataGridView.Columns[e.ColumnIndex].Name + "\" with index \"" + e.ColumnIndex + "\" not found!");
                    break;
            }
            rentalDataGridView.RefreshEdit();
        }

        /// <summary>
        /// The saveBtn_Click Method saves new lines and updates changes to the database on save button click.
        /// </summary>
        /// <param name="sender"> Contains a reference to the button that was clicked.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable newDT = dataTable.GetChanges();
                if (newDT != null)
                {
                    dataAdapter.Update(newDT);
                }
                MessageBox.Show("Information Updated", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                displayView();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Loads the RentalForm.
        /// </summary>
        /// <param name="sender"> Contains a reference to the object.</param>
        /// <seealso cref="System.Object"></seealso>
        /// <param name="e"> Contains the event data.</param>
        /// <seealso cref="System.EventArgs"></seealso>
        private void RentalForm_Load(object sender, EventArgs e)
        {
            displayView();
        }
    }
}
