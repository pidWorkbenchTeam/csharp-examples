﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Linq2SqlDatagridviewMultipleTablesExample
{
    public partial class frmLinq2SqlDatagridviewMultipleTables : Form
    {
        private DataClassesDataContext dataContext = new DataClassesDataContext();
        List<BookRentals> rentalList;
        private HashSet<int> hsInserted = new HashSet<int>();
        private HashSet<int> hsModified = new HashSet<int>();
        private HashSet<int> hsDeleted = new HashSet<int>();
        private int maxRentalId;

        public frmLinq2SqlDatagridviewMultipleTables()
        {
            InitializeComponent();
            //SELECT r.id, r.issueDate, p.firstname, p.lastname, b.title, b.isbn, rbs.returnDate
            //FROM Rentals r, Persons p, RentalBooksRelationship rbs, Books b
            //WHERE r.personId = p.id AND rbs.rentalId = r.id AND rbs.bookId = b.id
            //ORDER BY r.issueDate;

            maxRentalId = dataContext.GetTable<Rental>().Max(u => u.id) + 1;

            var query = (from rentals in dataContext.GetTable<Rental>()
                        from persons in dataContext.GetTable<Person>()
                        from books in dataContext.GetTable<Book>()
                        from rbs in dataContext.GetTable<RentalBooksRelationship>()
                        where ((rentals.personId == persons.id) &&
                               (rbs.rentalId == rentals.id) &&
                               (rbs.bookId == books.id))
                        orderby rentals.issueDate
                        select new BookRentals
                        {
                            RentalId = rentals.id,
                            IssueDate = rentals.issueDate,
                            Firstname = persons.firstname,
                            Lastname = persons.lastname,
                            BookTitle = books.title,
                            BookISBN = books.isbn,
                            ReturnDate = rbs.returnDate
                        }).ToList<BookRentals>();

            bookRentalsBindingSource.DataSource= query;
            dgvRentals.DataSource = bookRentalsBindingSource;
            rentalList = (List<BookRentals>)bookRentalsBindingSource.List;
        }

        public class BookRentals
        {
            public System.Int32 RentalId
            { get; set; }
            public System.Nullable<System.DateTime> IssueDate
            { get; set; }
            public System.String Firstname
            { get; set; }
            public System.String Lastname
            { get; set; }
            public System.String BookTitle
            { get; set; }
            public System.String BookISBN
            { get; set; }
            public System.Nullable<System.DateTime> ReturnDate
            { get; set; }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            print();
            // delete items
            foreach (int rentalId in hsDeleted)
            {
                if (!hsInserted.Contains(rentalId))
                { 
                var rental2Delete = ( from rentals in dataContext.GetTable<Rental>()
                                        where rentals.id == rentalId
                                        select rentals).SingleOrDefault();
                if (rental2Delete != null) // are entries to delete in db ?
                {
                    dataContext.Rentals.DeleteOnSubmit(rental2Delete);
                }
                else Console.WriteLine("rental with id " + rentalId + " could not be deleted cause it was not found!");
                }
            }
            // insert + modify items
            foreach (var rental in rentalList)
            {
                // insert
                if (hsInserted.Contains(rental.RentalId) && !hsDeleted.Contains(rental.RentalId))
                {
                    Rental rental2Insert = new Rental();
                    rental2Insert.id = rental.RentalId;
                    rental2Insert.issueDate = (DateTime)rental.IssueDate;
                    rental2Insert.personId = 0;
                    dataContext.Rentals.InsertOnSubmit(rental2Insert);
					
					RentalBooksRelationship rbs2Insert = new RentalBooksRelationship();
					rbs2Insert.bookId = 0;
					rbs2Insert.rentalId = rental.RentalId;
					rbs2Insert.returnDate = (DateTime)rental.ReturnDate; //Convert.ToDateTime("02.02.2222");
					dataContext.RentalBooksRelationships.InsertOnSubmit(rbs2Insert);
                } // modify
                else if (hsModified.Contains(rental.RentalId) && !hsInserted.Contains(rental.RentalId) && !hsDeleted.Contains(rental.RentalId))
                {
                    var rental2Modify = (from rentals in dataContext.GetTable<Rental>()
                                         where rentals.id == rental.RentalId
                                         select rentals).SingleOrDefault();

                    if (rental2Modify != null) // are entries to modify in db ?
                    {
                        rental2Modify.id = rental.RentalId;
                        rental2Modify.issueDate = (DateTime)rental.IssueDate;
                        rental2Modify.personId = 0;
                    }
                    else Console.WriteLine("rental with id " + rental.RentalId + " could not be modified cause it was not found!");
                }
            }
            //submit
            try
            {
                dataContext.SubmitChanges();
                MessageBox.Show("Data was saved successfully!", "Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error");
            }
            hsDeleted.Clear();
            hsInserted.Clear();
            hsModified.Clear();
            maxRentalId = dataContext.GetTable<Rental>().Max(u => u.id) + 1;
        } // btnSave_Click

        private void bookRentalsBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            if ((e.NewIndex > -1) && (bookRentalsBindingSource.List.Count > 0) && 
                (dgvRentals.RowCount > 0) && dgvRentals.Rows[e.NewIndex].Cells[0].Value != null)
            {
                if (e.ListChangedType == ListChangedType.ItemChanged)
                {
                    hsModified.Add(rentalList.ElementAt(e.NewIndex).RentalId);
                    //hsModified.Add((int)dgvRentals.Rows[e.NewIndex].Cells[0].Value);
                }
                else
                {
                    Console.WriteLine("--> bookRentalsBindingSource_ListChanged() Unhandled ListChangedType   : " + e.ListChangedType);
                }
            }
        }

        private void dgvRentals_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
        {
            e.Row.Cells[0].Value = maxRentalId;
            maxRentalId = maxRentalId + 1;
        }

        private void dgvRentals_UserAddedRow(object sender, DataGridViewRowEventArgs e)
        {
            hsInserted.Add((int)dgvRentals.CurrentRow.Cells[0].Value);
        }

        private void dgvRentals_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            hsDeleted.Add((int)e.Row.Cells[0].Value);
        }

        private void print()
        {
            System.Console.WriteLine("---------------------------------------------------------------------------");
            System.Console.WriteLine("---> Deleted:");
            foreach ( int id in hsDeleted ) System.Console.WriteLine("\t" + id);
            System.Console.WriteLine("---> Added:");
            foreach (int id in hsInserted) System.Console.WriteLine("\t" + id);
            System.Console.WriteLine("---> Modified:");
            foreach (int id in hsModified) System.Console.WriteLine("\t" + id);
            System.Console.WriteLine("---------------------------------------------------------------------------");
        }
    }
}
